var modules = require('modules');
var log = modules.use('log')(module);
var views = modules.use('views');

module.exports.get = function (req, res, next) {
    views.get('price-list').render('list',
        { handler: 'table', theme: false },
        function (err, html) {
            if (err) return next(err);

            res.render('page', {
                title: 'Стоимость Работ',
                variables: {
                    html: html
                }
            });
        }
    );
};

