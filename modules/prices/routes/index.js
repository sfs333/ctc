var modules = require('modules');
var switchTheme = modules.use('adminUI').middlewares.switchTheme;
var isAdmin = modules.use('admin').middlewares.isAdmin;

module.exports = function(app, cb) {
    //app.get('/admin/price/:id?', isAdmin, switchTheme, require('./price').get);
    app.get('/admin/price-list', isAdmin, switchTheme, require('./admin-list').get);
    app.get('/price-list', require('./list').get);
    cb();
}