var modules = require('modules');
var log = modules.use('log')(module);
var views = modules.use('views');

module.exports.get = function (req, res, next) {
    views.get('price-admin').render('adminList',
        { handler: 'table', theme: 'admin' },
        function (err, html) {
            if (err) return next(err);

            res.render('page', {
                title: 'Price list',
                variables: {
                    add: '<a href="#" class="popup-form-marker btn btn-primary link-price-add" data-title="Create Price" data-theme="admin" data-form-name="admin-price-form" data-id="" >CREATE</a>',
                    html: html
                }
            });
        }
    );
};

