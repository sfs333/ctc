var modules = require('modules');
var async = modules.use('async');
var views = modules.use('views');
var viewsTools = modules.use('viewsTools');

module.exports = function (cb) {
    async.parallel([
        function(cb) {
            views.add('price-admin', require('./price-admin'), function(err, view) {
                if (err) return cb(err);
                view.display('adminList', viewsTools.mgDisplay({modelName: 'Price'}));
                cb();
            });
        },
        function(cb) {
            views.add('price-list', require('./price-list'), function(err, view) {
                if (err) return cb(err);
                view.display('list', viewsTools.mgDisplay({modelName: 'Price'}));
                cb();
            });
        }
    ], cb);
}