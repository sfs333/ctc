module.exports = {
    pagination: {
        count: 15
    },
    fields: {
        title: {
            title: 'Название',
            type: 'string',
            params: {}
        },
        priceMin: {
            type: 'number',
            title: 'От',
            params: {}
        },
        priceMax: {
            type: 'number',
            title: 'До',
            params: {}
        },
        suffix: {
            type: 'string',
            title: 'цена/шт',
            params: {}
        }
    }
};