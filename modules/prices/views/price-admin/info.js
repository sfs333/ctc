module.exports = {
    pagination: {
        count: 15
    },
    fields: {
        title: {
            title: 'Title',
            type: 'string',
            params: {}
        },
        created: {
            title: 'Created',
            type: 'date',
            params: {}
        },
        type: {
            type: 'string',
            title: 'Type',
            params: {}
        },
        priceMin: {
            type: 'number',
            title: 'Price min',
            params: {}
        },
        priceMax: {
            type: 'number',
            title: 'Price max',
            params: {}
        },
        suffix: {
            type: 'string',
            title: 'Suffix',
            params: {}
        },
        actions: {
            title: 'Actions',
            type: 'markup',
            params: {
                template: 'views/partial/actions',
                actions: [
                    {
                        title: 'EDIT',
                        path: '/admin/price/%id',
                        class: 'popup-form-marker',
                        data: {'form-name': 'admin-price-form'}
                    },
                    {
                        title: 'DELETE',
                        path: '/admin/price/%id',
                        class: 'popup-form-marker',
                        data: {'form-name': 'admin-price-delete-form'}
                    }
                ]
            }
        }
    }
};