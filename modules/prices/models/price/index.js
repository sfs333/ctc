var modules = require('modules');
var log = modules.use('log')(module);
var mongoose = modules.use('mongoose');
var Schema = mongoose.Schema;

var name = 'Price';
var opts = {};
var info = {
    created: require('./fields/created'),
    updated: require('./fields/updated'),
    title: require('./fields/title'),
    suffix: require('./fields/suffix'),
    type: require('./fields/type'),
    priceMin: require('./fields/priceMin'),
    priceMax: require('./fields/priceMax')
}

var schema = new Schema(info, opts);
mongoose.model(name, schema);