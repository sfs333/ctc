module.exports = function () {
    this.name = 'admin-price-form';
    this.buttons = {
        submit: {title: 'save'},
        cancel: {title: 'back'}
    }
    this.getFields = function () {
        return {
            title: {
                type: 'text',
                title: 'Title',
                placeholder: 'Enter title',
                required: true
            },
            type: {
                type: 'select',
                title: 'Type',
                items: {
                    baths: 'Бани',
                    swimmingPools: 'Бассейны',
                    conditioning: 'Кондиционирование',
                    stairs: 'Лестницы',
                    fences: 'Ограждения',
                    furniture: 'Мебель'
                }
            },
            priceMin: {
                type: 'number',
                title: 'Price min',
                placeholder: 'Enter min price'
            },
            priceMax: {
                type: 'number',
                title: 'Price max',
                placeholder: 'Enter max price'
            },
            suffix: {
                type: 'text',
                title: 'Suffix',
                placeholder: 'Enter suffix'
            }
        }
    }
}