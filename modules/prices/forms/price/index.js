var modules = require('modules');
var log = modules.use('log')(module);
var mongooseForm = modules.use('mongooseForm');

new mongooseForm.build('Price', require('./info'), function(err, mf) {
    if (err) return log.error(err);
    require('./events')(mf);
});