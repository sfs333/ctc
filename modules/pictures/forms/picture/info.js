module.exports = function () {
    this.name = 'admin-picture-form';
    this.buttons = {
        submit: {title: 'save'},
        cancel: {title: 'back'}
    }
    this.getFields = function () {
        return {
            //title: {
            //    type: 'text',
            //    title: 'Title',
            //    placeholder: 'Enter title',
            //    required: true
            //},
            images: {
                type: 'image',
                title: 'Images',
                placeholder: 'Load images',
                fileDir: 'testPictures',
                maxSize: 15,
                multiple: true,
                saveOriginalName: true,
                presets: {
                    'fieldPreview' : {
                        smartSize: {
                            width: 100,
                            height: 60
                        }

                    },
                    //'expand' : {
                    //    smartSize: {
                    //        width: 150,
                    //        height: 420
                    //    }
                    //}

                    //}
                    //,
                    //'320_240_byFace' : {
                    //    cropByFace: {
                    //            width: 640,
                    //            height: 480
                    //    },
                    //    watermark: {
                    //        path: 'watermark_big.png',
                    //        position: 'center'
                    //    }
                    //},
                    '1280_720' : {
                        smartSize: {
                            width: 1280,
                            height: 720
                        },
                        watermark: {
                            path: 'watermark_big.png',
                            position: 'center'
                        }
                    },
                    '565_301' : {
                        smartSize: {
                            width: 565,
                            height: 301
                        },
                        watermark: {
                            path: 'watermark_big.png',
                            position: 'center'
                        }
                    },
                    '194_140' : {
                        smartSize: {
                            width: 194,
                            height: 140
                        },
                        watermark: {
                            path: 'watermark_big.png',
                            position: 'center'
                        }
                    }
                }
            }
        }
    }
}