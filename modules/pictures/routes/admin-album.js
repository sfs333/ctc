var modules = require('modules');
var log = modules.use('log')(module);
var views = modules.use('views');

module.exports.get = function (req, res, next) {

    res.render('page', {
        title: 'Picture Album',
        variables: {
            pictureForm: { form: 'admin-picture-form', id: req.params.id }
        }
    });

};

