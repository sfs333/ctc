var modules = require('modules');
var log = modules.use('log')(module);
var views = modules.use('views');

module.exports.get = function (req, res, next) {
    views.get('picture-admin').render('adminList',
        { handler: 'table', theme: 'admin' },
        function (err, html) {
            if (err) return next(err);

            res.render('page', {
                title: 'Picture list',
                variables: {
                    add: '<a href="#" class="popup-form-marker btn btn-primary link-picture-add" data-title="Create Picture" data-theme="admin" data-form-name="admin-picture-form" >CREATE</a>',
                    html: html
                }
            });
        }
    );
};

