var modules = require('modules');
var switchTheme = modules.use('adminUI').middlewares.switchTheme;
var isAdmin = modules.use('admin').middlewares.isAdmin;

module.exports = function(app, cb) {
    app.get('/admin/picture-list', isAdmin, switchTheme, require('./admin-list').get);
    app.get('/admin/picture/album/:id', isAdmin, switchTheme, require('./admin-album').get);
    cb();
}