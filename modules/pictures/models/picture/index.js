var modules = require('modules');
var log = modules.use('log')(module);
var mongoose = modules.use('mongoose');
var imgSchema = modules.use('formFile').imgSchema;
var Schema = mongoose.Schema;

var name = 'Picture';
var opts = {};

var info = {
    created: require('./fields/created'),
    title: require('./fields/title'),
    images: [imgSchema()]
}

var schema = new Schema(info, opts);
mongoose.model(name, schema);