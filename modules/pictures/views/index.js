var modules = require('modules');
var async = modules.use('async');
var views = modules.use('views');
var viewsTools = modules.use('viewsTools');

module.exports = function (cb) {
    async.parallel([
        function(cb) {
            views.add('picture-admin', require('./picture-admin'), function(err, view) {
                if (err) return cb(err);
                view.display('adminList', viewsTools.mgDisplay({modelName: 'Picture'}));
                cb();
            });
        }
    ], cb);
}