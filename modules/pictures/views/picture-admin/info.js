module.exports = {
    pagination: {
        count: 15
    },
    fields: {
        title: {
            title: 'Title',
            type: 'string',
            params: {}
        },
        created: {
            title: 'Created',
            type: 'date',
            params: {}
        },
        actions: {
            title: 'Actions',
            type: 'markup',
            params: {
                template: 'views/partial/actions',
                actions: [
                    {
                        title: 'ALBUMS',
                        path: '/admin/picture/album/%id'
                    },
                    {
                        title: 'EDIT',
                        path: '/admin/picture/%id',
                        class: 'popup-form-marker',
                        data: {'form-name': 'admin-picture-form'}
                    },
                    {
                        title: 'DELETE',
                        path: '/admin/picture/%id/delete',
                        class: 'popup-form-marker',
                        data: {'form-name': 'admin-picture-delete-form'}
                    }
                ]
            }
        }
    }
};