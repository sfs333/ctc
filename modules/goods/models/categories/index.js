var modules = require('modules');
var mongoose = modules.use('mongoose');
var log = modules.use('log')(module);
var Schema = mongoose.Schema;
var _ = require('underscore');

var name = 'GoodsCategories';
var opts = {};
var info = {
    created: require('./fields/created'),
    updated: require('./fields/updated'),
    title: require('./fields/title'),
    description: require('./fields/description'),
    weight: require('./fields/weight'),
    goods: [new Schema(require('./goods'))]
};

var schema = new Schema(info, opts);
require('./hooks')(schema);
require('./methods')(mongoose.model(name, schema));


// require('./update-all-url-aliases');

