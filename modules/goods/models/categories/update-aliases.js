var modules = require('modules');
var mongoose = modules.use('mongoose');
var _ = require('underscore');
var Link = mongoose.getModel('Link');
var transliterate = modules.use('transliterate');

var addItem = function (item, urlPrefix, aliasPrefix) {
    var url = (urlPrefix || '') + item.get('_id');
    var alias = (aliasPrefix || '/') + transliterate(item.get('title'));

    Link.findOne({ original: url }, function (err, link) {

        if (!link)
            link = new Link();

        link.set('original', url);
        link.set('alias', alias);
        link.save();
    });

    return alias;
};

module.exports = function (category) {
    var alias = addItem(category, '/goods/category/');

    _.each(category.get('goods') || [], function (item) {
        addItem(item, '/goods/', alias + '/');
    });
};