var modules = require('modules');
var mongoose = modules.use('mongoose');
var _ = require('underscore');

var updateAliases = require('./update-aliases');
var log = modules.use('log')(module);

mongoose.model('GoodsCategories').find({}, function (err, items) {
    if (err) return log.error(err);

    _.each(items, updateAliases);
});