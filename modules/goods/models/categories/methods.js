var modules = require('modules');

var updateAliases = require('./update-aliases');

var mongoose = modules.use('mongoose');
var Link = mongoose.getModel('Link');
var log = modules.use('log')(module);
var _ = require('underscore');

module.exports = function (category) {
    category.prototype.updateAliases = function () {
        updateAliases(this);
    };

    category.prototype.removeAliases = function () {
        var self = this;
        _.each(self.get('goods') || [], function (item) {
            self.removeGoodAlias.call(self, item);
        });

        Link.removeByOriginalName('/goods/category/' + self.get('_id'));
    };

    category.prototype.removeGoodAlias = function (good) {
        Link.removeByOriginalName('/goods/' + good.get('_id'));
    };
};