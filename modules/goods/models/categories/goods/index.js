var modules = require('modules');
var mongoose = modules.use('mongoose');
var Schema = mongoose.Schema;
var imgSchema = modules.use('formFile').imgSchema;

module.exports = {
    created: require('./fields/created'),
    updated: require('./fields/updated'),
    title: require('./fields/title'),
    description: require('./fields/description'),
    weight: require('./fields/weight'),
    picture: [imgSchema()],
    images: [imgSchema()]
    //images: [new Schema({
    //    name: require('./fields/name'),
    //    publicPath: require('./fields/publicPath'),
    //    path: require('./fields/path'),
    //    weight: require('./fields/weight'),
    //    size: require('./fields/size'),
    //    type: require('./fields/type'),
    //    uploaded: require('./fields/uploaded')
    //})]
};




