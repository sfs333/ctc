
module.exports = function (schema) {
    schema.post('save', function (category) {
        category.updateAliases();
    });
};