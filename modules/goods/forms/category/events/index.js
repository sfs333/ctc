var modules = require('modules');
var views = modules.use('views');;

module.exports = function(mf) {
    mf.formType.on('build', function(params, cb) {
        this.on('validate', function(cb) {
            var self = this;
            this.socket.request.loadUser(function(err, user) {
                if (err) return cb(err);

                if (!user.isAdmin)
                    return cb(new Error('Access denied'));
                cb();
            });
        });
        cb();
    });

    mf.formType.on('submit', function(cb) {
        this.response.actions.redirect = {url: 'admin/goods/categories'};
        cb();
    });
}