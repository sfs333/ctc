module.exports = function () {
    this.name = 'admin-goods-categories-form';
    this.buttons = {
        submit: {title: 'save'},
        cancel: {title: 'back'}
    }
    this.getFields = function () {
        return {
            title: {
                type: 'text',
                title: 'Title',
                placeholder: 'Enter title',
                required: true
            },
            description: {
                type: 'editor',
                title: false,
                maxSize: 5
            }
        }
    }
}