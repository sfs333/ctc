module.exports = function () {
    this.name = 'admin-goods-form';
    this.buttons = {
        submit: {title: 'save'},
        cancel: {title: 'back'}
    }
    this.getFields = function () {
        return {
            title: {
                type: 'text',
                title: 'Title',
                placeholder: 'Enter title',
                required: true
            },
            description: {
                type: 'editor',
                title: false,
                maxSize: 5
            },
            picture: {
                type: 'image',
                multiple: false,
                saveOriginalName: false,
                title: 'Picture',
                placeholder: 'Load picture',
                fileDir: 'goodsPictures',
                allowTypes: ['png', 'jpeg'],
                rewriteOriginal: 'prev_124x200',
		maxSize: 30,
                presets: {
                    fieldPreview: {
                        smartSize: {
                            width: 100,
                            height: 60
                        }
                    },
                    prev_124x200: {
                        smartSize: {
                            width: 124,
                            height: 200
                        }
                    }
                }
            }
        }
    }
}
