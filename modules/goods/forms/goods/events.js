var modules = require('modules');
var async = modules.use('async');
var util = modules.use('util');
var validator = modules.use('validator');
var GoodsCategories = modules.use('mongoose').model('GoodsCategories');

module.exports = function (info) {
    info.on('build', function (params, cb) {
        var self = this;
        if (self.values.id && validator.isMongoId(self.values.id)) {
            async.waterfall([
                function (cb) {
                    GoodsCategories.findOne({'goods._id': self.values.id}, cb);
                },
                function (doc, cb) {
                    if (!doc) return cb(new Error('Document not find'));
                    var goods = doc.get('goods').id(self.values.id);
                    if (!goods) return cb();

                    self.field('title').setDefault(goods.get('title'));
                    self.field('description').setDefault(goods.get('description'));


                    // self.field('picture').set(goods.get('picture') ? goods.get('picture')[0] : null);
                    self.field('picture').setDefault(goods.get('picture'));
                    cb();
                }
            ], cb);
        } else {
            cb();
        }
    });

    info.on('validate', function (cb) {
        var self = this;
        this.socket.request.loadUser(function (err, user) {
            if (err) return cb(err);

            if (!user.isAdmin)
                return cb(new Error('Access denied'));
            cb();
        });
    });


    info.on('submit', function (cb) {
        var self = this;
        async.waterfall([
            function (cb) {
                if (self.values.categoryId && validator.isMongoId(self.values.categoryId))
                    return GoodsCategories.findById(self.values.categoryId, cb);
                else if (self.values.id && validator.isMongoId(self.values.id))
                    return GoodsCategories.findOne({'goods._id': self.values.id}, cb);
                else
                    return cb(new Error('Not data for find document with goods'));
            },
            function (doc, cb) {
                if (!doc) return cb(new Error('Document not find'));
                self.values.categoryId = doc.get('id');
                var picture = self.get('picture');

                var good,
                    goods = doc.get('goods');
                if (self.values.id) {
                    good = goods.id(self.values.id);
                    if (!good) return cb();
                    good.set('title', self.get('title'));
                    good.set('description', self.get('description'));

                    if (picture)
                        good.set('picture', picture);

                } else {
                    goods.push({
                        title: self.get('title'),
                        description: self.get('description'),
                        picture: picture ? picture : []
                    });
                }

                doc.save(cb);
            }
        ], cb);
    });

    info.on('submit', function (cb) {
        this.response.actions.redirect = {url: 'admin/goods/category/' + this.values.categoryId + '/goods'};
        cb();
    });


}