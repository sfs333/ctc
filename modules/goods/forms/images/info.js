module.exports = function () {
    this.name = 'admin-goods-images-form';
    this.buttons = {
        submit: {title: 'save'},
        cancel: false
    }
    this.getFields = function () {
        return {
            images: {
                type: 'image',
                title: 'Images',
                placeholder: 'Load images',
                fileDir: 'goodsImages',
                maxSize: 30,
                multiple: true,
                saveOriginalName: false,
                rewriteOriginal: 'watermark_origin',
                presets: {
                    fieldPreview : {
                        smartSize: {
                            width: 100,
                            height: 60
                        }
                    },
                    size_194x140 : {
                        smartSize: {
                            width: 194,
                            height: 140
                        }
                    },
                    'watermark_565x301': {
                        resize: {
                            //width: 565,
                            height: 301
                        },
                        watermark: {
                            path: 'watermark_sm.png',
                            position: 'center'
                        }
                    },
                    'watermark_origin': {
                        maxSize: {
                            width: 1600,
                            height: 700
                        },
                        watermark: {
                            path: 'watermark_big.png',
                            position: 'center'
                        }
                    }
                }
            }
        }
    }
}
