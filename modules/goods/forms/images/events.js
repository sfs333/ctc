var modules = require('modules');
var async = modules.use('async');
var util = modules.use('util');
var validator = modules.use('validator');
var mongoose = modules.use('mongoose')
var GoodsCategories = mongoose.model('GoodsCategories');
var writeMultipleField = function(place, field) {
    var item;
    var key;
    if (util.isArray(field.add)) {
        field.add.forEach(function(info) {
            place.push(info);
        });
    }
    if (util.isArray(field.edit)) {
        field.edit.forEach(function(info) {
            item = place.id(info.id);
            if (item) {
                delete info.id;
                for (key in info)
                    item.set(key, info[key]);
            }
        });
    }
    if (util.isArray(field.delete)) {
        field.delete.forEach(function(info) {
            item = place.id(info.id);
            if (item) item.remove();
        });
    }
}

module.exports = function(info) {
    info.on('build', function(params, cb) {
        var self = this;
        if (self.values.id && validator.isMongoId(self.values.id)) {
            GoodsCategories.aggregate([
                {$unwind: "$goods"},
                {$match: {'goods._id': mongoose.Types.ObjectId(self.values.id)}},
                {$project: {
                    images: '$goods.images'
                }},
                {$unwind: "$images"},
                {$sort : { 'images.uploaded' : -1, 'images.weight' : 1 } },
                {$project: {
                    path: '$images.path',
                    publicPath: {value: '$images.presets.fieldPreview.public', params: {full: '$images.publicPath'}},
                    name: '$images.name',
                    _id: '$images._id',
                    uploaded: '$images.uploaded',
                    type: '$images.type',
                    size: '$images.size',
                    presets: '$images.presets',
                    weight: '$images.weight'
                }},
            ], function(err, data) {
                // console.log(data);
                self.field('images').setDefault(data);
                cb();
            });
            //async.waterfall([
            //    function(cb) {
            //        GoodsCategories.findOne({'goods._id' : self.values.id}, cb);
            //    },
            //    function(doc, cb) {
            //        if (!doc) return cb(new Error('Document not find'));
            //        var goods = doc.get('goods').id(self.values.id);
            //        if (!goods) return cb();
            //
            //        //console.log(goods.get('images'));
            //
            //        //self.field('images').setDefault(goods.get('images'));
            //
            //    }
            //], cb);
        } else {
            cb();
        }
    });

    info.on('validate', function (cb) {
        var self = this;
        this.socket.request.loadUser(function(err, user) {
            if (err) return cb(err);

            if (!user.isAdmin)
                return cb(new Error('Access denied'));
            cb();
        });
    });


    info.on('submit', function (cb) {
        var self = this;

        async.waterfall([
            function(cb) {
                if (self.values.id && validator.isMongoId(self.values.id))
                    return GoodsCategories.findOne({'goods._id' : self.values.id}, cb);
                else
                    return cb(new Error('Not data for find document with goods'));
            },
            function(doc, cb) {
                if (!doc) return cb(new Error('Document not find'));

                if (self.values.id) {
                    var good = doc.get('goods').id(self.values.id);
                    if (!good) return cb();
                    writeMultipleField(good.get('images'), self.get('images'));
                }
                doc.save(cb);
            }
        ], cb);
    });


    info.on('submit', function (cb) {
        this.response.actions.redirect = {url: 'admin/goods/images/' + this.values.id };
        cb();
    });


}