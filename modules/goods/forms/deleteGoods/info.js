module.exports = function() {
    this.name = 'admin-goods-delete-form';
    this.buttons = {
        submit: {title: 'Yes'},
        cancel: {title: 'cancel'}
    }
    this.getFields = function() {
        return {
            infoText: {
                type: 'markup',
                title: '',
                value: 'Are you sure?'
            }
        }
    }
}