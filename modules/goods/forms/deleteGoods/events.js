var modules = require('modules');
var async = modules.use('async');
var util = modules.use('util');
var validator = modules.use('validator');
var mongoose = modules.use('mongoose');
var GoodsCategories = mongoose.model('GoodsCategories');

module.exports = function(info) {
    info.on('validate', function (cb) {
        var self = this;
        this.socket.request.loadUser(function(err, user) {
            if (err) return cb(err);

            if (!user.isAdmin)
                return cb(new Error('Access denied'));
            cb();
        });
    });
    info.on('validate', function (cb) {
        if (!validator.isMongoId(this.values.id))
            return cb(new Error('Not correct id'));
        cb();
    });
    info.on('submit', function (cb) {
        var self = this;
        GoodsCategories.findOne({ 'goods._id' : mongoose.Types.ObjectId(self.values.id)}, function(err, doc) {
            if (err) return cb(err);

            var goods = doc.get('goods').id(self.values.id);
            self.response.actions.redirect = {url: util.format('admin/goods/category/%s/goods', doc.get('id'))};

            if (goods) {
                doc.removeGoodAlias(goods);
                goods.remove();
                return doc.save(cb);
            }
            cb();
        });
    });
}