var modules = require('modules');

var Info = require('./info');
Info.prototype = modules.use('formType');
var info = new Info();

require('./events')(info);
modules.use('form').registerForm('admin-goods-delete-form', info);