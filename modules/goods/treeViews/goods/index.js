var modules = require('modules');

var goods = modules.use('treeView').add('goods', {});
goods.getData = require('./getData');
goods.save = require('./save');
goods.addView(require('./view'));