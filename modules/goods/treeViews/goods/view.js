module.exports = {
    title: {
        title: 'Title',
        type: 'string',
        params: {
            link: {
                prefix: 'goods'
            }
        }
    },
    created: {
        title: 'Created',
        type: 'date',
        params: {}
    },
    actions: {
        title: 'Actions',
        type: 'markup',
        params: {
            template: 'views/partial/actions',
            actions: [
                {
                    title: 'IMAGES',
                    path: '/admin/goods/images/%id'
                },
                {
                    title: 'EDIT',
                    path: '/admin/goods/category/%id/edit',
                    class: 'popup-form-marker',
                    data: {'form-name': 'admin-goods-form'}
                },
                {
                    title: 'DELETE',
                    path: '/admin/goods/images/%id',
                    class: 'popup-form-marker',
                    data: {'form-name': 'admin-goods-delete-form'}
                }
            ]
        }
    }
}