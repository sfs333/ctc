var modules = require('modules');
var mongoose = modules.use('mongoose');
var GoodsCategories = mongoose.model('GoodsCategories');
var validator = modules.use('validator');

module.exports = function(context, cb) {
    if (!validator.isMongoId(context.categoryId))
        return cb(new Error('Not find categoryId'));

    GoodsCategories.aggregate([
            {$match: {_id: mongoose.Types.ObjectId(context.categoryId)}},
            {$unwind: "$goods"},
            {$project: {
                weight: '$goods.weight',
                title: '$goods.title',
                created: '$goods.created',
                _id: '$goods._id'
            }}
    ],
    cb);
}