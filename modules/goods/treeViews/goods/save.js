var modules = require('modules');
var mongoose = modules.use('mongoose');
var GoodsCategories = mongoose.model('GoodsCategories');
var validator = modules.use('validator');
var util = modules.use('util');
var async = modules.use('async');

module.exports = function(info, cb) {
    var items = util.isArray(info.items) ? info.items : [];
    var categoryId = info.context.categoryid;
    if (!categoryId || !validator.isMongoId(categoryId))
        return cb(new Error('Incorrect category id'));
    GoodsCategories.findById(categoryId, function(err, doc) {
        if (err) return cb(err);
        var goods = {};
        items.forEach(function(item) {
            goods = doc.get('goods').id(item.id);
            if (goods)
                goods.weight = (item.weight ? Number(item.weight) : 0);
        });
        doc.save(cb);
    });
}