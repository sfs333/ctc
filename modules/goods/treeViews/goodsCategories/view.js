module.exports = {
    title: {
        title: 'Title',
        type: 'string',
        params: {
            link: {
                prefix: 'goods/category'
            }
        }
    },
    created: {
        title: 'Created',
        type: 'date',
        params: {}
    },
    actions: {
        title: 'Actions',
        type: 'markup',
        params: {
            template: 'goods/categoryActions'
        }
    }
}