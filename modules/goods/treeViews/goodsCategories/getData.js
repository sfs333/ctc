var modules = require('modules');
var GoodsCategories = modules.use('mongoose').model('GoodsCategories');
module.exports = function(context, cb) {
    GoodsCategories.aggregate([
            {$project: {
                title: 1,
                description: 1,
                weight: 1,
                created: 1,
                parent: { $concat: [ "root" ] }
            }}
    ],
    cb);
}