var modules = require('modules');

var goodsCategories = modules.use('treeView').add('goodsCategories', {});
goodsCategories.getData = require('./getData');
goodsCategories.save = require('./save');
goodsCategories.addView(require('./view'));