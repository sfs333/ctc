var modules = require('modules');
var mongoose = modules.use('mongoose');
var GoodsCategories = mongoose.model('GoodsCategories');
var validator = modules.use('validator');
var util = modules.use('util');
var async = modules.use('async');

module.exports = function(info, cb) {
    var items = util.isArray(info.items) ? info.items : [];
    var workers = [];
    var addToUpdate = function(item) {
        if (!item.id || !validator.isMongoId(item.id))
            return false;

        workers.push(function(cb) {
            var params = { weight: item.weight ? Number(item.weight) : 0 };
            GoodsCategories.update({_id: mongoose.Types.ObjectId(item.id)}, params, cb);
        });
    };
    items.forEach(function(item) {
        addToUpdate(item);
    });
    async.parallel(workers, cb);
}