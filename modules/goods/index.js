var modules = require('modules');
var async = modules.use('async');
//var publicDepends = modules.use('publicDepends');

require('./models');
require('./forms');
module.exports.run = function (app, cb) {
    require('./routes')(app);
    require('./treeViews');

    /*
    var mongoose = modules.use('mongoose');
    var log = modules.use('log')(module);
    var GoodsCategories = mongoose.model('GoodsCategories');
    async.waterfall([
        function (cb) {
            setTimeout(cb, 1000);
        },
        function (cb) {
            GoodsCategories.aggregate([
                {$match: {'goods.files._id': mongoose.Types.ObjectId('555b5ee7b0d99ffa31ff1926')}},
                {$unwind: "$goods"},
                //{$unwind: "$files"},
                {$project: {
                    files: '$goods.files'
                }},
                {$unwind: "$files"},
                {$sort: { 'files.title' : 1 }},
            ], cb);
        },
        function(items, cb) {
            console.log(items);
            cb();
        }
    ], function(err) {
        if (err) return log.error(err);
    });

*/
/*
    async.waterfall([
        function (cb) {
            setTimeout(cb, 1000);
        },
        function (cb) {
            GoodsCategories.findById('555b27af510911232122bb67', cb);
        },
        function (doc, cb) {
            doc
                .get('goods')
                .id('555b27e9510911232122bb69')
                .get('files').push({ title: '3214' });

            doc.save(cb);
        },
        function (doc, n, cb) {
            console.log(doc);
            cb();
        }
    ]);
*/

    cb();
};

//publicDepends.addDependency({
//    goods: {
//        scripts: ['js/admin/menu/list'],
//        dependencies: ['app']
//    }
//});