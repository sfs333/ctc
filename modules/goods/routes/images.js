var modules = require('modules');
var log = modules.use('log')(module);
var validator = modules.use('validator');
var async = modules.use('async');
var GoodsCategories = modules.use('mongoose').model('GoodsCategories');

module.exports.get = function (req, res, next) {
    var id = req.params.id || null;
    if (validator.isMongoId(id)) {
        async.waterfall([
            function(cb) {
                GoodsCategories.findOne({'goods._id' : id}, cb);
            },
            function(doc, cb) {
                if (!doc) return cb(new Error('Document not find'));


                cb(null, doc.get('goods').id(id));
            }
        ], function(err, goods) {
            res.render('page', {
                title: goods.get('title') + ' (images)',
                variables: {
                    pictureForm: { form: 'admin-goods-images-form', id: id }
                }
            });
        });
    } else {
        next();
    }
};

