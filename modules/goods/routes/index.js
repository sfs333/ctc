var modules = require('modules');
var switchTheme = modules.use('adminUI').middlewares.switchTheme;
var isAdmin = modules.use('admin').middlewares.isAdmin;

module.exports = function(app) {
    app.get('/goods/:id', require('./goods').get);
    app.get('/admin/goods/categories', isAdmin, switchTheme, require('./admin-category-list').get);
    app.get('/admin/goods/category/:category/goods', isAdmin, switchTheme, require('./admin-goods-list').get);
    app.get('/admin/goods/images/:id', isAdmin, switchTheme, require('./images').get);

    app.get('/goods/category/:category', require('./categoryItems').get);
    app.get('/goods/:id', require('./goods').get);
    app.get('/gallery', require('./gallery').get);
}