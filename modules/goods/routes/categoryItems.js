var modules = require('modules');
var validator = modules.use('validator');
var mongoose = modules.use('mongoose');
var async = modules.use('async');
var GoodsCategories = mongoose.model('GoodsCategories');



module.exports.get = function(req, res, next) {
    if (!validator.isMongoId(req.params.category))
        return next();
    async.parallel([
        function(cb) {
            GoodsCategories.aggregate([
                    {$match: {_id: mongoose.Types.ObjectId(req.params.category)}},
                    {$unwind: "$goods"},
                    {$sort:{ 'goods.weight' : 1 }},
                    {$project: {
                        weight: '$goods.weight',
                        title: '$goods.title',
                        created: '$goods.created',
                        picture: '$goods.picture.presets.prev_124x200.public',
                        description: '$goods.description',
                        _id: '$goods._id'
                    }}
            ], cb);
        },
        function(cb) {
            GoodsCategories.findById(req.params.category, {title: 1, description: 1}, cb);
        }
    ], function(err, docs) {
        var goods = docs[0];
        var category = docs[1];
        if (err || !category) return next(err);

        res.render('goods/categoryItems', {
            title: category.get('title'),
            goods: goods,
            category: category

        });
    });
};