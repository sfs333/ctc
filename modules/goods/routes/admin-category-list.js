var modules = require('modules');
var log = modules.use('log')(module);
var views = modules.use('views');
var treeView = modules.use('treeView');
//categoryId
module.exports.get = function (req, res, next) {
    res.dependency('treeView');

    treeView.get('goodsCategories').render({theme: 'admin', colCount: '3'}, function(err, html) {
        if (err) return next(err);

        res.render('page', {
            title: 'Goods Categories',
            variables: {
                add: '<a href="/admin/goods" class="popup-form-marker btn btn-primary link-goods-category-add" data-title="Create Category" data-theme="admin" data-form-name="admin-goods-categories-form" data-id="" >CREATE</a>',
                html: html
            }
        });
    });

};
