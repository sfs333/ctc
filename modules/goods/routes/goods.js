var modules = require('modules');
var validator = modules.use('validator');
var mongoose = modules.use('mongoose');
var async = modules.use('async');
var GoodsCategories = mongoose.model('GoodsCategories');

module.exports.get = function(req, res, next) {
    if (!validator.isMongoId(req.params.id))
        return next();

    res.dependency('async');
    async.parallel([
        function(cb) {
            GoodsCategories.aggregate([
                {$unwind: "$goods"},
                {$match: {'goods._id': mongoose.Types.ObjectId(req.params.id)}},
                {$project: {
                    images: '$goods.images'
                }},
                {$unwind: "$images"},
                //{$sort : { 'images.weight' : 1 } },
                {$sort : { 'images.uploaded' : -1 } },
                {$project: {
                    path: '$images.presets.watermark_565x301.public',
                    full: '$images.presets.watermark_origin.public',
                    name: '$images.name'
                }},
            ], cb);
        },
        function(cb) {
            GoodsCategories.aggregate([
                {$unwind: "$goods"},
                {$match: {'goods._id': mongoose.Types.ObjectId(req.params.id)}},
                {$project: {
                    title: '$goods.title',
                    created: '$goods.created',
                    description: '$goods.description',
                    _id: '$goods._id'
                }}
            ], cb);
        }
    ], function(err, data) {
        if (err || !data[1][0]) return next(err);

        res.render('goods/item', {
            title: data[1][0].title,
            goods: data[1][0],
            images: data[0]
        });
    });
};