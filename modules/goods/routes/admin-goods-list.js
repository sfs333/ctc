var modules = require('modules');
var log = modules.use('log')(module);
var views = modules.use('views');
var async = modules.use('async');
var util = modules.use('util');
var treeView = modules.use('treeView');
var validator = modules.use('validator');
var GoodsCategories = modules.use('mongoose').model('GoodsCategories');
//categoryId
module.exports.get = function (req, res, next) {
    var category = null;
    var categoryId = req.params.category;
    if(!validator.isMongoId(categoryId))
        return next();

    res.dependency('treeView');
    async.waterfall([
        function(cb) {
            GoodsCategories.findById(categoryId, cb);
        },
        function(doc, cb) {
            if (!doc)
                return cb(new Error('Category Goods not found'));
            category = doc;
            cb();
        },
        function(cb) {
            treeView.get('goods').render({categoryId: categoryId, theme: 'admin', colCount: '3'}, cb);
        }
    ], function(err, html) {
        if (err) return next(err);

        res.render('page', {
            title: util.format('Goods for %s', category.get('title')),
            variables: {
                add: '<a href="/admin/goods" class="popup-form-marker btn btn-primary link-goods-category-add" data-title="Create Category" data-theme="admin" data-form-name="admin-goods-form" data-category-id="' + req.params.category + '" >CREATE</a>',
                html: html
            }
        });
    });
};
