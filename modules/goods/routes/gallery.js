var modules = require('modules');
var validator = modules.use('validator');
var mongoose = modules.use('mongoose');
var async = modules.use('async');
var util = modules.use('util');
var GoodsCategories = mongoose.model('GoodsCategories');

module.exports.get = function (req, res, next) {


    GoodsCategories.aggregate([
        {$unwind: "$goods"},
        //{$match: {'goods._id': mongoose.Types.ObjectId(req.params.id)}},
        {
            $project: {
                images: '$goods.images',
                categoryName: '$goods.title'
            }
        },
        {$unwind: "$images"},
        {$sort: {'images.uploaded': -1}},
        {$limit: 50},
        {
            $project: {
                path: '$images.presets.size_194x140.public',
                full: '$images.publicPath',
                name: '$images.name',
                categoryName: 1
            }
        },
    ], function (err, images) {
        if (err) return next(err);

        var html = '<div class="row image-nav"><ul class="nav side-nav">';
        images.forEach(function (img) {
            html += util.format('<li style="height: auto" class="col-lg-2 col-md-2 col-sm-3 col-xs-12"><a href="#" class="img-nav-link"><img src="/%s" style="height: 140px; max-width: 285px" data-id="%s" title="%s" /></a></li>', img.path, img._id, img.categoryName);
        });
        html += '</ul></div>';


        res.render('goods/gallery', {
            title: 'Галерея',
            images: images
        });
    });
}