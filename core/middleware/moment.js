var moment = require('moment');

module.exports = function(req, res, next) {
    moment.locale('ru');
    res.locals.moment = moment;
    next();
};