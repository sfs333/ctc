var moduleManager = require('moduleManager');
var log = moduleManager.use('log')(module);
var util = require('util');
var mongoose = require('mongoose');
var config = moduleManager.use('config');
var clc = require('cli-color');

mongoose.connect(config.get('mongoose:uri'), config.get('mongoose:options'));
if (config.get('mongoose:logs')) {
    //mongoose.set('debug', true);
    mongoose.set('debug', function (collectionName, method, query, doc) {
        console.log(clc.white('mongoose') + ' ' + clc.magentaBright(collectionName));
        var qs = util.inspect(query);
        qs.split("\n").forEach(function(str) {
            console.log(clc.cyan(str));
        });
        console.log("\n");
    });
}

mongoose.Error.messages.general.required = "Field `{PATH}` is required.";

mongoose.rebuildModel = function(name, schema) {
    if ( typeof this.connection.models[name] == 'function') {
        delete this.connection.models[name];
        this.model(name, schema);
    }
}

mongoose.checkUnique = function(modelName, field, value, callback) {
    var model = this.model(modelName);
    var q = {};
    if (!model)
        return callback(new Error('Not find model ' + modelName));
    q[field] = value;
    model
        .find(q)
        .limit(1)
        .select('_id')
        .exec(function(err, results) {
            if (err) {
                log.error(err);
                return callback(false);
            }
            callback(results.length != 1);
        });
}

mongoose.isObjectId = function(n) {
    return this.Types.ObjectId.isValid(n);
}

mongoose.idOrDoc = function(type, value, callback) {
    if (!value)
        return callback(new Error('not find value in idOrDocument'));

    var callback = (typeof callback == 'function') ? callback : function(){};
    var Document = mongoose.model(type);
    if (value instanceof Document)
        return callback(null, value);

    if (mongoose.isObjectId(value))
        return Document.findById(value, callback);

    callback(null, null);
}

mongoose.getModel = function(name) {
    var model = null;
    try {
        model = this.model(name);
    } catch(e) {
        return false;
    }
    return model;
}

//mongoose.Document.prototype.validateField = function(path, callback) {
//    var schemaPath = this.schema.path(path);
//    if (!schemaPath)
//        return callback(new Error('not correct path'));
//
//    schemaPath.doValidate(this[path].email, callback);
//}

module.exports = mongoose;