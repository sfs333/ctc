var bytes = require('bytes');
var util = require('util');
var moduleManager = require('moduleManager');
var moment = require('moment');
var clc = require('cli-color');
var log = moduleManager.use('log')(module);

var logger = {
    37: log.info,
    10: log.info,
    184: log.warn,
    196: log.error
}

var ipNames = {
    '195.88.53.143': 'Ruslan',
    '192.168.1.112': 'Ruslan',
    '178.151.220.11': 'Jeffrey',
    '83.30.134.37': 'Jeffrey 2'
}

//var remoteAddress = function(req) {
//    if (req.connection && req.connection.remoteAddress)
//        return req.connection.remoteAddress.replace(/[\: f]/g, '');
//
//    var splitH = (req.headers['x-forwarded-for'] || '').split(',')[0];
//    if (splitH) return splitH;
//
//    var sock = req.socket;
//    if (sock.socket) return sock.socket.remoteAddress;
//    return sock.remoteAddress;
//};

var colorStr = function (str, color) {
    return '\x1B[' + color + 'm' + str + '\x1b[0m' + ' ';
}

var getStatusColor = function(status) {
    if (status == 200)
        return 10;
    else if (status >= 500)
        return 196;
    else if (status >= 400)
        return 184;
    else if (status >= 300)
        return 37;
    else
        return 196;
}

var getMsColor = function(ms) {
    if (ms <= 10)
        return 10;
    else if (ms <= 20)
        return 2;
    else if (ms <= 35)
        return 11;
    else if (ms <= 60)
        return 142;
    else
        return 124;
}

var getNameIP = function(ip) {
    return (typeof ipNames[ip] == 'string') ? ipNames[ip] : false;
}

var logWrite = function(req, res) {
    var status = res.statusCode
        , color = getStatusColor(status)
        , ip = req.getIP()
        , ipName = false;

    var ms = (new Date - req._startTime);

    var str = util.format('%s %s %s %s %s %s',
        clc.green(moment(req._startTime).format("HH:mm:ss MM:DD:YY")),
        clc.cyan(ip),
        clc.xterm(color)(status),
        clc.xterm(getMsColor(ms))(ms  + 'ms'),
        clc.blackBright(req.method),
        clc.blackBright((req.originalUrl || req.url))
    );

    logger[color](str);
};

module.exports = function(req, res, next) {
    req._startTime = new Date;

    function logRequest() {
        logWrite(req, res);
        res.removeListener('finish', logRequest);
        res.removeListener('close', logRequest);
    }

    res.on('finish', logRequest);
    res.on('close', logRequest);
    next();
};