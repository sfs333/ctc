var util = require('util');

var systemError = function(opt) {
    opt = (typeof opt === 'object') ? opt : {};
    this.errors = {};
    this.prefix = opt.prefix || 'SYSTEM';
    this.emptyMessage = opt.emptyMessage || 'no message text';
}

var proto = systemError.prototype;

proto.add = function(key) {
    return new Error(util.format('%s: %s', this.prefix, this.get(key)));
}

proto.get = function(key) {
    return this.errors[key] ? this.errors[key] : this.emptyMessage;
}

proto.set = function(key, message) {
    this.errors[key] = util.format('%s', message);
}

module.exports = function(opt) {
    return new systemError(opt);
};