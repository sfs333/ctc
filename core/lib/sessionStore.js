var session = require('express-session');

var RedisStore = require('connect-redis')(session);
var store = new RedisStore();
module.exports = store;
