var modules = require('modules');
var validator = require('validator');
var async = require('async');
var popup = modules.use('popup');
var ContactMessages = modules.use('mongoose').model('ContactMessages');

popup.add('contactMessage', {
    template: 'renderer',
    theme: 'admin',
    getVariables: function(context, cb) {
        if (!context.socket)
            return new Error('Socket not find in popup context');
        if (!validator.isMongoId(context.id))
            return new Error('Socket not find in popup context');

        async.waterfall([
            context.socket.request.loadUser,
            function(user, cb) {
                if (!user.isAdmin)
                    return cb(new Error('Access denied'));

                ContactMessages.findById(context.id, {message: 1}, cb);
            },
            function(doc, cb) {
                if (!doc) return cb(new Error('Document not exist'));
                cb(null, { variables: { message: {container: doc.get('message')} }});
            }

        ], cb);
    }
});