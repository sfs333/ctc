var async = require('async');
require('./models');
require('./forms');

module.exports.run = function(app, next) {
    require('./routes')(app);
    async.parallel([
        function(cb) {
            require('./views')(cb);
        },
        function(cb) {
            require('./defaults')(cb);
        }
    ], next);
}