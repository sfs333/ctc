var modules = require('modules');
var async = require('async');
var variables = modules.use('variables');
var defaultTemplate = 'Message from <%- name %> (contact: <%- contact %>), <br><br> <%- message %>';

module.exports = function(cb) {
    async.parallel([
        function(cb) {
            variables.get('feedbackReceivers', function(err, value) {
                if (err) return cb(err);
                if (value && value.length > 0)
                    return cb();

                variables.set('feedbackReceivers', modules.use('config').get('adminEmail'), cb);
            });
        },
        function(cb) {
            variables.get('feedbackTemplate', function(err, value) {
                if (err) return cb(err);
                if (value && value.length > 0)
                    return cb();

                variables.set('feedbackTemplate', defaultTemplate, cb);
            });
        }
    ], cb);

}