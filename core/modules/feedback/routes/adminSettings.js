module.exports.get = function(req, res, next) {
    res.render('page', {
        title: 'Feedback',
        variables: {
            form: {form: 'feedback-settings-form'}
        }
    });
};