var modules = require('modules');
var log = modules.use('log')(module);
var views = modules.use('views');

module.exports.get = function (req, res, next) {
    views.get('ContactMessages').render('adminList',
        { handler: 'table', theme: res.locals.theme },
        function (err, html) {
            if (err) return next(err);

            res.render('page', {
                title: 'Feedback Messages',
                variables: { html: html }
            });
        }
    );
};
