var modules = require('modules');
var isAdmin = modules.use('admin').middlewares.isAdmin;
var switchTheme = modules.use('adminUI').middlewares.switchTheme;

module.exports = function (app, cb) {
    app.use(require('./parse-synonyms'));
    app.get('/admin/link-synonym-list', isAdmin, switchTheme, require('./admin-list').get);

    cb();
};