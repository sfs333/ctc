var Link = require('mongoose').model('Link');

module.exports = function (req, res, next) {
    var originalLinkExist = Link.getOriginal(req._parsedUrl.pathname);

    if (originalLinkExist) {
        req.urlRewritten = req.url;
        req.url = originalLinkExist + (req._parsedUrl.search || '');
    }

    return next();
};

