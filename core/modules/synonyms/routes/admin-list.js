var modules = require('modules');
var log = modules.use('log')(module);
var views = modules.use('views');

module.exports.get = function (req, res, next) {
    views.get('link-synonym-admin').render('adminList',
        { handler: 'table', theme: 'admin' },

        function (err, html) {
            if (err) return next(err);

            res.render('page', {
                title: 'Synonyms of Links',
                variables: {
                    add: '<a href="#" class="popup-form-marker btn btn-primary link-synonym-add" data-title="Create Alias" data-theme="admin" data-form-name="admin-link-synonym-form" data-id="" >CREATE</a>',
                    html: html
                }
            });
        }
    );
};

