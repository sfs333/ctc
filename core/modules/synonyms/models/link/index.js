var _ = require('underscore');
var modules = require('modules');
var log = modules.use('log')(module);
var mongoose = modules.use('mongoose');
var Schema = mongoose.Schema;

var name = 'Link';
var opts = {};
var info = {
    created: require('./fields/created'),
    updated: require('./fields/updated'),
    original: require('./fields/original'),
    alias: require('./fields/alias')
};

var storage = {
    direct: {},
    reverse: {}
};

var schema = new Schema(info, opts);
var Link = mongoose.model(name, schema);


schema.post('save', function (link) {
    Link.addToCache(link);
});

schema.post('find', function(docs) {
    console.log('this fired after you run a find query', docs);
});

_.extend(Link, {
    storage: {
        direct: {},
        reverse: {}
    },

    getOriginal: function (alias) {
        return this.storage.reverse[alias];
    },

    getAlias: function (original) {
        return this.storage.direct[original];
    },

    addToCache: function (link) {

        var existAlias = this.getAlias(link.get('original'));
        if (existAlias)
            delete this.storage.reverse[existAlias];

        this.storage.direct[link.get('original')] = link.get('alias');
        this.storage.reverse[link.get('alias')] = link.get('original');
    },
    fillCache: function (links) {
        _.each(links, this.addToCache, this);
    },
    removeByOriginalName: function (original) {
        this.findOne({ original: original }, function (err, link) {
            if (err) return log.error(err);

            if (link) link.remove();
        });
    }
});

Link.find({}, function (err, results) {
    if (err) return log.error(err);

    Link.fillCache(results);
});
