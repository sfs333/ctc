var modules = require('modules');
var async = modules.use('async');
var views = modules.use('views');
var viewsTools = modules.use('viewsTools');

module.exports = function(cb) {

    views.add('link-synonym-admin', require('./link-synonym-admin'), function(err, view) {
        if (err) return cb(err);

        view.display('adminList', viewsTools.mgDisplay({modelName: 'Link'}));
        cb();
    });
};