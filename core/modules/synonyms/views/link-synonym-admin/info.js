module.exports = {
    pagination: {
        count: 100
    },

    fields: {
        original: {
            title: 'Original',
            type: 'string',
            params: {}
        },
        alias: {
            title: 'Alias',
            type: 'string',
            params: {}
        },
        created: {
            title: 'Created',
            type: 'date',
            params: {}
        },
        actions: {
            title: 'Actions',
            type: 'markup',
            params: {
                template: 'views/partial/actions',
                actions: [
                    {
                        title: 'EDIT',
                        path: '/admin/synonym-link/%id',
                        class: 'popup-form-marker',
                        data: {'form-name': 'admin-link-synonym-form'}
                    },
                    {
                        title: 'DELETE',
                        path: '/admin/synonym-link/%id',
                        class: 'popup-form-marker',
                        data: {'form-name': 'admin-link-synonym-delete-form'}
                    }
                ]
            }
        }
    }
};