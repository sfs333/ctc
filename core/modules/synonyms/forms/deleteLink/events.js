var modules = require('modules');
var async = modules.use('async');
var util = modules.use('util');
var validator = modules.use('validator');
var Link = modules.use('mongoose').model('Link');

module.exports = function(info) {
    info.on('validate', function (cb) {
        this.socket.request.loadUser(function(err, user) {
            if (err) return cb(err);

            if (!user.isAdmin)
                return cb(new Error('Access denied'));

            cb();
        });
    });
    info.on('validate', function (cb) {
        if (!validator.isMongoId(this.values.id))
            return cb(new Error('Not correct id'));
        cb();
    });
    info.on('submit', function (cb) {
        Link.findOneAndRemove({_id: this.values.id}, cb);
    });
    info.on('submit', function (cb) {
        this.response.actions.redirect = {url: 'admin/link-synonym-list'};
        cb();
    });
}