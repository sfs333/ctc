module.exports = function () {
    this.name = 'admin-link-synonym-form';
    this.buttons = {
        submit: {title: 'save'},
        cancel: {title: 'back'}
    };

    this.getFields = function () {

        return {
            original: {
                type: 'text',
                title: 'Original Link',
                placeholder: 'Enter original link',
                required: true
            },
            alias: {
                type: 'text',
                title: 'Alias',
                placeholder: 'Enter alias for link',
                required: true
            }
        }
    };
};