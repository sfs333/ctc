var modules = require('modules');
var publicDepends = modules.use('publicDepends');

module.exports = require('./lib');
module.exports.run = function(app, cb) {
    require('./io')(app.get('io'));
    require('./views');
    cb();
}
publicDepends.addDependency({
    treeView: {
        scripts: ['js/treeView/jquery-sortable', 'js/treeView/index'],
        dependencies: ['app', 'jq'],
        static: true
    }
});