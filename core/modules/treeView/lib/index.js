var ThreeView = require('./ThreeView');


var storage = {};
var add = function(name, params) {
    return storage[name] = new ThreeView(name, params);

};
var get = function(name) {
    return storage[name] || false;
};

module.exports.add = add;
module.exports.get = get;