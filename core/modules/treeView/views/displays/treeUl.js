var async = require('async');
var mongoose = require('mongoose');
var modules = require('modules');
var async = require('async');
var treeView = modules.use('treeView');

module.exports = function (context, cb) {
    var item = treeView.get(context.name);
    async.waterfall([
        function(cb) {
            item.getData(context, cb);
        },
        function(items, cb) {
            cb(null, {items: items });
        }
    ], cb);
}
