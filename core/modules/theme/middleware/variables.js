var theme = require('./../index');
var modules = require('modules');
var config = modules.use('config');

module.exports = function(req, res, next) {
    res.locals.theme = {name: config.get('theme'), path: theme.getTheme()};
    next();
}