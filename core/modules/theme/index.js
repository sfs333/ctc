var modules = require('modules');
var path = require('path');
var config = modules.use('config');

module.exports.run = function (app, cb) {
    require('./render')(app);
    app.use(require('./middleware/variables'));
    app.use(require('./middleware/adminNav'));
    cb();
}

module.exports.getAdminTheme = function() {
    return path.join(__dirname, '../../../themes/' + config.get('adminTheme'));
}

module.exports.getTheme = function() {
    return path.join(__dirname, '../../../themes/' + config.get('theme'))
}

modules.use('publicDepends').addDependency({
    adminPanel: {
        scripts: ['js/adminPanel/formButton'],
        dependencies: ['app', 'jq'],
        static: true
    }
});


