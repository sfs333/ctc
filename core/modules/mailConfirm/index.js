module.exports = require('./lib');

module.exports.run = function (app, cb) {
    require('./routes/index')(app);
    cb();
}