var modules = require('modules');
var async = require('async');
var popup = modules.use('popup');
var form = modules.use('form');
var log = modules.use('log')(module);

popup.add('form', {
    template: 'popup/form',
    theme: 'admin',
    params: {dialogClass: 'popup-form', width: 800 },
    getVariables: function(context, cb) {
        async.waterfall([
            function(cb) {
                if (!context.formName)
                    return cb (new Error('Not find form-name data'));

                var formName = context.formName;
                delete context.formName;
                delete context.name;
                delete context.title;
                context.isPopUp = true;
                form.createFormItem(formName, context, cb);
            },
            function(form, cb) {
                form.render(cb);
            },
            function(html, cb) {
                cb(null, {form: html});
            }
        ], cb);
    }
});