var modules = require('modules');
var async = modules.use('async');
var views = modules.use('views');
var viewsTools = modules.use('viewsTools');

module.exports = function(cb) {

    views.add('meta-tags-list-admin', require('./admin-list'), function(err, view) {
        if (err) return cb(err);

        view.display('adminList', viewsTools.mgDisplay({modelName: 'MetaTag'}));
        cb();
    });
};