var modules = require('modules');
var log = modules.use('log')(module);
var views = modules.use('views');

module.exports.get = function (req, res, next) {
    views.get('meta-tags-list-admin').render('adminList',
        { handler: 'table', theme: 'admin' },

        function (err, html) {
            if (err) return next(err);

            res.render('page', {
                title: 'Meta-Tags',
                variables: {
                    add: '<a href="#" class="popup-form-marker btn btn-primary meta-tag-add" data-title="Add meta-tag" data-theme="admin" data-form-name="admin-meta-tag-form" data-id="" >CREATE</a>',
                    html: html
                }
            });
        }
    );
};

