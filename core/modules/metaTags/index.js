var modules = require('modules');
var async = modules.use('async');

require('./models');
require('./forms');
// require('./locals');

module.exports.run = function (app, cb) {

    async.series([
        function(cb) {
            require('./routes')(app, cb);
        },
        function(cb) {
            require('./views')(cb);
        }

    ], cb);
};




