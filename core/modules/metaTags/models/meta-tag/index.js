var modules = require('modules');
var log = modules.use('log')(module);
var mongoose = modules.use('mongoose');
var Schema = mongoose.Schema;

var name = 'MetaTag';
var opts = {};
var info = {
    created: require('./fields/created'),
    updated: require('./fields/updated'),
    path: require('./fields/path'),
    title: require('./fields/title'),
    keywords: require('./fields/keywords'),
    description: require('./fields/description'),
};

var schema = new Schema(info, opts);
mongoose.model(name, schema);
