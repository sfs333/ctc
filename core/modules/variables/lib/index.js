var modules = require('modules');
var async = require('async');
var util = require('util');
var mongoose = require('mongoose');
var log = modules.use('log')(module);
var redisStorage = modules.use('redisStorage');
var config = modules.use('config');

var Variables = mongoose.model('Variables');
var error = modules.use('error')({prefix: 'VARIABLES'});
error.set('keyNoStr', 'key param should be is string type.');
error.set('getNfc', 'getter no function.');

var func = function(err) {
    if (err) return log.error(err);
}

var VariablesClass = function() {}

var proto = VariablesClass.prototype;

proto.isString = function(str) {
    return (typeof str === 'string');
}
proto.set = function(key, value, cb) {
    var self = this;
    if (!this.isString(key))
        return cb(error.add('keyNoStr'));
    if (typeof cb !== 'function')
        cb = func;
    if (typeof value !== 'boolean')
        value = util.format('%s', value);
    else
        value = value ? '1' : '0';
    async.parallel([
        function(cb) {
            self.setModel(key, value, cb);
        },
        function(cb) {
            self.setCache(key, value, cb);
        }
    ], cb);
}

proto.get = function(key, cb) {
    var self = this;
    if (!this.isString(key))
        return cb(error.add('keyNoStr'));
    if (typeof cb !== 'function')
        cb = func;

    var value = null;
    var getters = [self.getCache, self.getModelValue, self.getConfig];
    var count = getters.length;
    var level = 0;

    async.whilst(
        function() {
            return ((level < count) && (value === null));
        },
        function(cb) {
            if (typeof getters[level] !== 'function')
                return cb(error.add('getNfc'));

            getters[level].apply(self, [key, function(err, resValue) {
                if (err) return cb(err);
                value = resValue;

                level++;
                cb();
            }]);
        },
        function(err) {
            if (err) return log.error(err);

            if ((value === null) || (value === undefined)) {
                return cb(null, null);
            }
            switch (level) {
                case 1:
                    cb(null, value);
                    log.info(util.format('getted %s from cache', key));
                    break;
                case 2:
                    log.info(util.format('getted %s from model', key));
                    self.setCache(key, value, function(err) {
                        cb(err, value);
                    });
                    break;
                case 3:
                    log.info(util.format('getted %s from config', key));
                    self.set(key, value, function(err) {
                        cb(err, value);
                    });
                    break;
                default:
                    break;
            }
        }
    );
}

proto.setModel = function(key, value, cb) {
    this.getModel(key, function(err, variable) {
        if (err) return cb(err);
        if (!variable) {
            variable = new Variables();
            variable.set('key', key);
        }

        variable.set('value', value);
        variable.save(cb);
    });
}

proto.getModel = function(key, cb) {
    Variables.findOne({key: key },{ value:1 }, cb);
}

proto.getModelValue = function(key, cb) {
    this.getModel(key, function(err, variable) {
       if (err) return cb(err);
       cb(null, ((variable !== null) ? variable.get('value') : null));
    });
}

proto.cacheKey = function(key) {
    return 'variable_' + key;
}

proto.setCache = function(key, value, cb) {
    redisStorage.set(this.cacheKey(key), value, cb);
}

proto.getCache = function(key, cb) {
    redisStorage.get(this.cacheKey(key), cb);
}

proto.getConfig = function(key, cb) {
    cb(null, config.get('variables:' + key));
}
module.exports = new VariablesClass();
