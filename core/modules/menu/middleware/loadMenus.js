var moduleManager = require('moduleManager');
//var user = require('mongoose').model('user');
var menu = require('./../index');

module.exports = function(req, res, next) {
    //if (!req.isPage) return next();

    menu.renderLayoutMenus(function(err, menus) {
        if (err) return next(err);

        res.locals.menu = menus;
        next();
    });
}