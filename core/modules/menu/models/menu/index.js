var modules = require('modules');
var log = modules.use('log')(module);
var mongoose = modules.use('mongoose');
var Schema = mongoose.Schema;

var name = 'Menu';
var opts = {};
var info = {
    created: require('./fields/created'),
    updated: require('./fields/updated'),
    title: require('./fields/title'),
    addToLayout: require('./fields/addToLayout'),
    description: require('./fields/description'),
    type: require('./fields/type')
}

var schema = new Schema(info, opts);
require('./virtual')(schema);
mongoose.model(name, schema);