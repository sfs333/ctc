module.exports = function(schema) {
    schema.virtual('name')
        .get(function() {
            return this.get('title').replace(' ', '_').toLowerCase();
        })
        .set(function(v) {});
}
