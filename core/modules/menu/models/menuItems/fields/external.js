module.exports = {
    type: Boolean,
    required: true,
    default: false
}