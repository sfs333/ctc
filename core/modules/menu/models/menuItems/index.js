var moduleManager = require('moduleManager');
var log = moduleManager.use('log')(module);
var mongoose = moduleManager.use('mongoose');
var Schema = mongoose.Schema;

var name = 'MenuItems';
var opts = {};
var info = {
    title: require('./fields/title'),
    path: require('./fields/path'),
    weight: require('./fields/weight'),
    parent: require('./fields/parent'),
    menu: require('./fields/menu'),
    external: require('./fields/external')
}

var schema = new Schema(info, opts);
require('./middleware')(schema);
mongoose.model(name, schema);