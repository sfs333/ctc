module.exports = function () {
    this.name = 'admin-menu-item-form';
    this.buttons = {
        submit: {title: 'save'},
        cancel: {title: 'back'}
    }
    this.getFields = function () {
        return {
            title: {
                type: 'text',
                title: 'Title',
                placeholder: 'Enter title',
                required: true
            },
            path: {
                type: 'text',
                title: 'Path',
                placeholder: 'Enter path'
            },
            menu: {
                type: 'select',
                title: 'Parent Menu',
                items: {}
            }
        }
    }
}