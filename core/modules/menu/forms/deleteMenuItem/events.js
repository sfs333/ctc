var modules = require('modules');
var async = require('async');
var util = require('util');
var validator = modules.use('validator');
var MenuItems = require('mongoose').model('MenuItems');

module.exports = function(info) {
    info.on('validate', function (cb) {
        var self = this;
        this.socket.request.loadUser(function(err, user) {
            if (err) return cb(err);

            if (!user.isAdmin)
                return cb(new Error('Access denied'));
            cb();
        });
    });
    info.on('validate', function (cb) {
        if (!validator.isMongoId(this.values.id))
            return cb(new Error('Not correct id'));
        cb();
    });
    info.on('submit', function (cb) {
        MenuItems.findOneAndRemove({_id: this.values.id}, cb);
    });
    info.on('submit', function (cb) {
        this.response.actions.redirect = {url: ''};
        cb();
    });
}