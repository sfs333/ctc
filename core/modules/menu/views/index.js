var modules = require('modules');
var async = require('async');
var log = modules.use('log')(module);
var views = modules.use('views');
var viewsTools = modules.use('viewsTools');

module.exports = function (cb) {
    async.parallel([
        function(cb) {
            views.add('menu', require('./menu'), function(err, view) {
                if (err) return cb(err);
                view.display('adminList', viewsTools.mgDisplay({modelName: 'Menu'}));
                cb();
            });
        }
    ], cb);
}