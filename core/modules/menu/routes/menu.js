var modules = require('modules');

module.exports.get = function(req, res, next) {
    var id = req.params.id || false;

    res.render('menu/menu', {
        title: id ? 'Edit menu' : 'Create menu',
        id: id
    });
};

