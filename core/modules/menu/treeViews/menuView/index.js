var modules = require('modules');
var log = modules.use('log')(module);

var menuView = modules.use('treeView').add('menuView', {});
menuView.getData = require('./../menuItems/getData');

menuView.addView(require('./view'), function(err, view) {
    if (err) return log.error(err);
    view.enableLayout = false;
});