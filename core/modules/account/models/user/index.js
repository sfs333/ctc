var moduleManager = require('moduleManager');
var log = moduleManager.use('log')(module);
var mongoose = moduleManager.use('mongoose');
var Schema = mongoose.Schema;

var name = 'user';
var opts = {};
var info = {
        //name: require('./fields/name'),
        email: require('./fields/email'),
        hashedPassword: require('./fields/hashedPassword'),
        salt: require('./fields/salt'),
        created: require('./fields/created'),
        auth: require('./fields/auth'),
        blocked: require('./fields/blocked')
}

var schema = new Schema(info, opts);
require('./static')(schema.statics);
require('./methods')(schema.methods);
require('./virtual')(schema);
require('./middleware')(schema);

mongoose.model(name, schema);