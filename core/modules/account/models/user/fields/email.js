var mongoose = require('mongoose');
module.exports = {
    type: String,
    unique: true,
    validate: {
        validator: function (value, respond) {
            if (!this.isNew)
                return respond(true);

            mongoose.checkUnique('user', 'email', value, respond);
        },
        msg: 'User with email {VALUE} already exist'
    }
}