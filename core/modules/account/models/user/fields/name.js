var mongoose = require('mongoose');
module.exports = {
    type: String,
    unique: true,
    required: true,
    validate: {
        validator: function (value, respond) {
            mongoose.checkUnique('user', 'name', value, respond);
        },
        msg: 'User {VALUE} already exist'
    }
}


