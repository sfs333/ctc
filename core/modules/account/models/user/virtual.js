module.exports = function(schema) {
    schema.virtual('pass')
        .get(function() { return ''; })
        .set(function(password) {
            if (password && password.length > 0) {
                this.salt = Math.random() + '';
                this.hashedPassword = this.encryptPassword(password + '');
            }
        });
}
