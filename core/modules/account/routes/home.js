module.exports.get = function (req, res, next) {
    if (req.session.confirmationAccount && (typeof req.session.confirmationAccount == 'object')) {
        res.jsValues.message = req.session.confirmationAccount.confirm ? 'You successfully confirmed your account' : 'This account has already been activated';
        req.session.confirmationAccount = false;
    }
    next();
};