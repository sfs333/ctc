module.exports = function(app) {
    app.get('/profile', require('./profile').get);
    app.get('/login', require('./login').get);
    app.get('/logout', require('./logout').get);
    app.get('/registration', require('./registration').get);
    app.get('/', require('./home').get);
}