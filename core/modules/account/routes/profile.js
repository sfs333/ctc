var moduleManager = require('moduleManager');
var mongoose = require('mongoose');
var async = require('async');
var util = require('util');
var User = mongoose.model('user');

module.exports.get = function(req, res, next) {
    if (!req.user.isAuth()) {
        var err = new Error('Access denied');
        err.status = 403;
        return next(err);
    }
    //res.dependency('profilePage');

    var profile = req.user.getProfile();
    res.render('page', {
        title: 'Profile',
        variables: {
            name: util.format('<p><strong>name: </strong> %s</p>', req.user.get('name')),
            email: util.format('<p><strong>email: </strong> %s</p>', req.user.get('email')),
            created: util.format('<p><strong>created: </strong> %s</p>', req.user.get('created')),
            bday: util.format('<p><strong>bday: </strong> %s</p>', profile.get('bday')),
            country: util.format('<p><strong>country: </strong> %s</p>', profile.get('country'))
        }
    });
};