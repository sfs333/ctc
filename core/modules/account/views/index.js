var moduleManager = require('moduleManager');
var async = require('async');
var log = moduleManager.use('log')(module);
var views = moduleManager.use('views');

module.exports = function (cb) {
    async.parallel([
        function(cb) {
            views.add('profile', require('./user/index'), function(err, view) {
                if (err) return cb(err);
                view.display('main', require('./user/displays/main'));
                cb();
            });
        }
    ], cb);
}