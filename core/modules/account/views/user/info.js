module.exports = {
    pagination: false,
    fields: {
        picture:{
            title: 'Picture',
            type: 'image',
            params: {
                width: 250
            }
        },
        name: {
            title: 'Chat Name',
            type: 'string'
        },
        email: {
            title: 'Email',
            type: 'string'
        },
        sex: {
            title: 'Sex',
            type: 'string'
        },
        country: {
            title: 'Country',
            type: 'string'
        },
        bday: {
            title: 'Bday',
            type: 'date'
        },
        created: {
            title: 'Created',
            type: 'date'
        }
    }
};