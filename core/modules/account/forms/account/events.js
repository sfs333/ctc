module.exports = function(mf) {
    mf.getDocument = function(params, callback) {
        if (typeof params.socket != 'object')
            return callback(new Error('no socket in load profile form'));

        params.socket.request.loadUser(callback);
    }
}