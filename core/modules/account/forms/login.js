var moduleManager = require('moduleManager');
var clc = require('cli-color');
var log = moduleManager.use('log')(module);
var User = require('mongoose').model('user');
var Form = moduleManager.use('form');
var formType = moduleManager.use('formType');

var Info = function () {
    this.name = 'login-form';
    this.buttons = {
        submit: {title: 'Login'}
    }
    //this.getClientParams = function() {
    //    return {
    //        'error-wrapper-selector': '.form-login.form .warning'
    //    };
    //}
    this.getFields = function () {
        return {
            email: {
                type: 'text',
                title: 'Email',
                placeholder: 'Enter email',
                required: true
                //description: '(Email)'
            },
            pass: {
                type: 'pass',
                title: 'Pass',
                placeholder: 'Enter pass',
                required: true
            }
        }
    }
}

Info.prototype = formType;
var info = new Info();

info.on('build', function (params, cb) {
    var self = this;
    this.socket.request.sessionManager.get(function(err, session) {
        if (err) return cb(err);
        if (session.user) {
            delete self.buttons.submit;
            self.fields = {
                infoText: self.createField({
                    type: 'markup',
                    name: 'infoText',
                    title: '',
                    value: 'You is auth now'
                })
            };
            return cb();
        }
        cb();
    });
});

info.on('validate', function (callback) {
    var form = this;
    User.findOne({email: this.get('email')}, function (err, user) {
            if (err) {
                log.error(err);
                return callback({ message: 'Error authorize!' });
            }
            if (!user)
                return callback({ message: 'User don`t exist!',field: 'email' });
            if (user.get('blocked'))
                return callback({ message: 'Your account blocked', field: 'email' });
            if (!user.checkPassword(form.get('pass')))
                return callback({ message: 'Don`t correct pass', field: 'pass' });

            form.user = user;
            callback(null);
    });
});

info.on('submit', function (callback) {
    if (!this.user || !this.user.get('id')) {
        log.error('Error load user, object empty or not exist!');
        return callback(new Error('Error in system.'));
    }
    this.user.set('auth', true);
    this.user.save(callback);
});

info.on('submit', function (callback) {
    this.session.user = this.user.id;
    callback(null);
});

info.on('submit', function (callback) {
    log.info('User ' + clc.cyan(this.user.get('email')) + ' success login');
    this.response.actions.redirect = {url: (this.user.isAdmin() ? 'admin' : '')}
    callback();
});

Form.registerForm('user-login-form', info);