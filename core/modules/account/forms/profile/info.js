module.exports = function() {
    this.name = 'user-profile-form';
    this.buttons = {
        submit: {title: 'Save'},
        cancel: false
    }
    this.getFields = function() {
        return {
            'profile.0.picture': {
                type: 'file',
                title: 'Picture',
                placeholder: 'Load picture',
                maxSize: 3,
                minSize: 0.01,
                fileDir: 'pics',
                allowTypes: ['png', 'jpeg']
            },
            'profile.0.chatName': {
                type: 'text',
                title: 'Chat Name',
                placeholder: 'Enter your Chat name'
                //activeValidate: true
            },
            'profile.0.description': {
                type: 'text',
                title: 'About you',
                placeholder: 'Enter your short information'
            },
            'profile.0.sex': {
                type: 'select',
                title: 'Gender',
                items: {
                    male : 'Male',
                    female : 'Female',
                    couples : 'Couples',
                    trans : 'Transexual'
                }
            },
            'profile.0.country': {
                type: 'select',
                title: 'Country',
                items: require('./../../data/country')
            },
            'profile.0.age': {
                type: 'number',
                title: 'Your age',
                placeholder: 'Enter your age'
            },
            'profile.0.languages': {
                type: 'text',
                title: 'How languages you know (use just "," like separator)',
                placeholder: 'Enter languages'
            }
        }
    }
}