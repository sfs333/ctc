module.exports = function() {
    this.name = 'user-logout-form';
    this.buttons = {
        submit: {title: 'Log Out'},
        cancel: {title: 'cancel'}
    }
    this.getFields = function() {
        return {
            infoText: {
                type: 'markup',
                title: '',
                value: 'Are you sure?'
            }
        }
    }
}