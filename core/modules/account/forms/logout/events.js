var modules = require('modules');
var async = require('async');
var util = require('util');
var validator = modules.use('validator');
var Menu = require('mongoose').model('Menu');

module.exports = function(info) {
    info.on('validate', function (cb) {
        var self = this;
        this.socket.request.loadUser(function(err, user) {
            if (err) return cb(err);

            if (!user.isAuth())
                return cb(new Error('Access denied. You not auth!'));
            self.autoSaveSession = false;
            self.user = user;
            cb();
        });
    });

    info.on('validate', function (cb) {
        console.log('logout');
        this.session.destroy();
        this.user.set('auth', false);
        this.user.save(cb);
    });

    info.on('submit', function (cb) {
        this.response.actions.redirect = {url: '#'};
        cb();
    });
}