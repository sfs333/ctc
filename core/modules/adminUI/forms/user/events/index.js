module.exports = function(mf) {
    mf.formType.on('build', function(params, callback) {
        if (params.id)
            this.fields.pass.required = false;

        this.on('validate', function(callback) {
            this.socket.request.loadUser(function(err, user) {
                if (err) return callback(err);

                if (!user.isAdmin)
                    return callback(new Error('Access denied'));

                callback();
            });
        });
        callback();
    });
    mf.formType.on('submit', function(callback) {
        this.response.actions.redirect = {url: ''};
        callback(null);
    });
}