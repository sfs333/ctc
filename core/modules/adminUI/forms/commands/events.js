var modules = require('modules');
var async = require('async');
var util = require('util');
var variables = modules.use('variables');
var shell = require('shelljs');
var config = modules.use('config');

module.exports = function(info) {
    info.on('validate', function (cb) {
        var self = this;
        this.socket.request.loadUser(function(err, user) {
            if (err) return cb(err);

            if (!user.isAdmin)
                return cb(new Error('Access denied'));
            cb();
        });
    });

    info.on('submit', function (cb) {
        this.response.actions.timeoutAction = {
            time: 2000,
            action: 'redirect',
            attributes: {
                url: ''
            }
        };
        cb();
        setTimeout(function() {
            shell.exec('forever restart ' + config.get('foreverProcessID'));
        }, 200);
    });
}