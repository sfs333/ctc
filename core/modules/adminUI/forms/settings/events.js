var modules = require('modules');
var async = require('async');
var path = require('path');
var util = require('util');
var variables = modules.use('variables');
var app = modules.use('app');

module.exports = function(info) {
    info.on('validate', function (cb) {
        var self = this;
        this.socket.request.loadUser(function(err, user) {
            if (err) return cb(err);

            if (!user.isAdmin)
                return cb(new Error('Access denied'));
            cb();
        });
    });

    info.on('build', function(params, cb) {
        var self = this;
        async.parallel([
            function(cb) {
                variables.get('offline', function(err, value) {
                    if (err) return cb(err);
                    if (value)
                        self.field('offline').setDefault(value === '1');
                    cb();
                });
            },
            function(cb) {
                variables.get('siteName', function(err, value) {
                    if (err) return cb(err);
                    if (value)
                        self.field('siteName').setDefault(value);
                    cb();
                });
            },
            function(cb) {
                variables.get('siteSlogan', function(err, value) {
                    if (err) return cb(err);
                    if (value)
                        self.field('siteSlogan').setDefault(value);
                    cb();
                });
            },
            function(cb) {
                variables.get('logoFileName', function(err, value) {
                    self.field('logo').setDefault([{
                        path: path.join(app.get('publicPath'), value),
                        publicPath: value,
                        name: 'logo',
                        type: 'image/jpeg',
                        size: '0'
                    }]);
                    cb();
                });
            }
        ], cb);
    });

    info.on('submit', function (cb) {
        return variables.set('offline', this.get('offline'), cb);
        cb();
    });

    info.on('submit', function (cb) {
        var siteName = this.get('siteName');
        if (siteName) return variables.set('siteName', siteName, cb);
        cb();
    });

    info.on('submit', function (cb) {
        var siteSlogan = this.get('siteSlogan');
        if (siteSlogan) return variables.set('siteSlogan', siteSlogan, cb);
        cb();
    });

    info.on('submit', function (cb) {
        var logo = this.get('logo');
        if (logo && logo.path) return variables.set('logoFileName', (logo.publicPath), cb);
        cb();
    });

    info.on('submit', function (cb) {
        this.response.actions.redirect = {url: ''};
        cb();
    });
}