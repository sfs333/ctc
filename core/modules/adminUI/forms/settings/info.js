module.exports = function() {
    this.name = 'site-settings-form';
    this.buttons = {
        submit: {title: 'Save'},
        cancel: false
    }
    this.getFields = function() {
        return {
            offline: {
                type: 'checkbox',
                title: 'Offline Mode',
                description: 'Disable site for all users except 192.168.1.*',
                fieldClass: 'toggle-checkbox'
            },
            siteName: {
                type: 'text',
                title: 'Site name',
                placeholder: 'Enter your site name'
            },
            siteSlogan: {
                type: 'text',
                title: 'Site Slogan',
                placeholder: 'Enter your slogan from the site'
            },
            logo: {
                type: 'file',
                title: 'Logo',
                placeholder: 'Load logo',
                maxSize: 3,
                minSize: false,
                fileDir: 'logo',
                fileName: 'logoDefault',
                allowTypes: ['png', 'jpeg']
            }
        }
    }
}