var moduleManager = require('moduleManager');
var async = require('async');
var log = moduleManager.use('log')(module);
var views = moduleManager.use('views');

module.exports = function (cb) {
    async.parallel([
        function(cb) {
            views.add('user', require('./user/index'), function(err, view) {
                if (err) return cb(err);
                view.display('adminList', require('./user/displays/adminList'));
                cb();
            });
        },
        function(cb) {
            views.add('accountTabs', require('./accountTabs/index'), function(err, view) {
                if (err) return cb(err);
                view.display('main', require('./accountTabs/displays/main'));
                cb();
            });
        }
    ], cb);
}