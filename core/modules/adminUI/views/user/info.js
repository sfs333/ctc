module.exports = {
    pagination: {
        count: 15
    },
    fields: {
        name: {
            title: 'Screen Name',
            type: 'string',
            params: {
                link: {prefix: 'admin/accounts/view'}
            }
        },
        email: {
            title: 'Email',
            type: 'string',
            params: {
                link: {prefix: 'admin/accounts/view'}
            }
        },
        auth: {
            title: 'Online',
            type: 'boolean'
        },
        blocked: {
            title: 'Status',
            type: 'boolean',
            params: {'true': 'not active', 'false': 'active'}
        },
        created: {
            title: 'Created',
            type: 'date',
            params: {
                format: 'DD.MM.YYYY'
            }
        },
        actions: {
            title: 'Actions',
            type: 'markup',
            params: {
                template: 'markups/usersActions'
            }
        }
    }
};