var modules = require('modules');
var publicDepends = modules.use('publicDepends');
require('./forms');

module.exports.run = function(app, cb) {
    require('./popups');
    require('./routes')(app);
    require('./io')(app.get('io'));
    require('./views')(cb);
};

module.exports.middlewares = {
    switchTheme: require('./middleware/switchTheme')
}

publicDepends.addDependency({
    adminUsers: {
        scripts: ['js/admin/users/index']
    }
});