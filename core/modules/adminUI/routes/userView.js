var moduleManager = require('moduleManager');
var mongoose = require('mongoose');
var async = require('async');
var User = mongoose.model('user');
var config = moduleManager.use('config');
var views = moduleManager.use('views');

module.exports.get = function(req, res, next) {
    //res.dependency('');
    var tabsView = views.get('accountTabs');

    var id = req.params.id;
    if (!id || !(mongoose.isObjectId(id)))
        return next();

    var config = moduleManager.use('config');
    async.waterfall([
        function(cb) {
            User.findById(id, function(err, user) {
                if (err) return cb(err);
                if (!user) return cb(new Error('User not find'));

                cb(null, user);
            });
        },
        //function(user, cb) {
        //    async.parallel([
        //        function(cb) { cb(null, user); },
        //        function(cb) {
        //            tabsView.render('main', { handler: 'menuButtons', user: user }, cb );
        //        }
        //    ], cb);
        //}
    ], function(err, user) {
        if (err) return next(err);

        //var user = info[0];
        //var tabs = info[1];
        res.render('userView', {
            title: 'View - ' + user.get('name'),
            account: user
            //tabs: tabs
        });
    });
};



