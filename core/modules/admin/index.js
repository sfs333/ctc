module.exports = require('./lib/index');
require('./models/index');

module.exports.middlewares = {
    isAdmin: require('./middleware/isAdmin')
}
module.exports.run = function(app, cb) {
    cb();
}