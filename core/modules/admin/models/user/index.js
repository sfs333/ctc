var moduleManager = require('moduleManager');
var mongoose = moduleManager.use('mongoose');
var User = mongoose.model('user');

User.schema.plugin(function (schema, options) {
    require('./statics')(schema.statics);
    require('./methods')(schema.methods);
    //mongoose.rebuildModel('user', schema);
});