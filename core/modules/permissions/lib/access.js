var async = require('async');
var util = require('util');
var mongoose = require('mongoose');
var moduleManger = require('moduleManager');
var log = moduleManger.use('log')(module);
var serialEE = moduleManger.use('serialEE');

var Access = function() {
    this.run();
}
util.inherits(Access, serialEE);
var proto = Access.prototype;
var superRun = proto.run;


proto.run = function() {
    superRun.apply(this);
}
proto.perm = function(perm, user, cb) {


    cb();
}
proto.check = function(perm, user, cb) {
    var User = mongoose.model('user');
    if (typeof cb !== 'function')
        cb = function(err) { if (err) log.error(err); };
    if (typeof perm !== 'string')
        return cb(new Error('Not correct permission name!'));
    var self = this;
    async.waterfall([
        function(cb) { User.getter(user, cb); },
        function(user, cb) {
            if (user.isAdmin()) return cb(null, true);
            async.series([
                function(cb) { self.perm(perm, user, cb) }, //Check permission in db
                function(cb) { self.emit(perm, { user: user, access: self }, cb) } //Allow any listener redefine access
            ], function(err) {
                if (err) {
                    log.warn(err);
                    log.warn(util.format('Access "%s" denied for user "%s"', perm, user.get('name')));
                    return cb(null, false);
                }
                cb(null, true);
            });
        }
    ], cb);
}

var access = new Access();
module.exports = access;


//setTimeout(function() {
//
//    console.log('A  C  C  E   S   S');
//    access.use('broadcastFree', function(cb) {
//
//        cb();
//    });
//
//    access.use('broadcastFree', function(cb) {
//
//        cb();
//    });
//    access.custom = 'Value!';
//
//
//    access.check('broadcastFree', '54c639a7db5b732c204c91ab', function(err, access) {
//        if (err) return log.error(err);
//        console.log(access);
//    });
//
//
//}, 1000);
