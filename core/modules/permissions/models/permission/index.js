var moduleManager = require('moduleManager');
var log = moduleManager.use('log')(module);
var mongoose = moduleManager.use('mongoose');
var Schema = mongoose.Schema;

var info = {
    name: require('./fields/name'),
    sysName: require('./fields/sysName'),
    description: require('./fields/description'),
    module: require('./fields/module')
}

var schema = new Schema(info, {});
require('./static')(schema.statics);
//require('./methods')(schema.methods);

mongoose.model('Permission', schema);