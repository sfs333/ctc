var mongoose = require('mongoose');
var util = require('util');
var moduleManager = require('moduleManager');
var log = moduleManager.use('log')(module);

module.exports = function(staticMethods) {
    staticMethods.add = function(info, cb) {
        if (typeof cb !== 'function')
            cb = function(err) { if (err) log.error(err); }
        if ((typeof info !== 'object') || (!info.sysName))
            return cb(new Error('Not correct info for add permission'));

        log.info(util.format('Register permission "%s" from "%s" module'), info.sysName, info.module);
        var Permission = mongoose.model('Permission');
        Permission.findOne({sysName: String(info.sysName)}, function(err, perm) {
            if (err) return cb(err);
            if (perm) return cb();
            var permission = new Permission(info);
            permission.save(cb);
        });
    }
}