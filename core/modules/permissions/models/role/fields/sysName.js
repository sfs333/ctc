var mongoose = require('mongoose');
module.exports = {
    type: String,
    unique: true,
    required: true,
    validate: {
        validator: function (value, respond) {
            mongoose.checkUnique('Permission', 'sysName', value, respond);
        },
        msg: 'Permission {VALUE} already exist'
    }
}


