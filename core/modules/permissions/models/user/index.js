var moduleManager = require('moduleManager');
var log = moduleManager.use('log')(module);
var mongoose = moduleManager.use('mongoose');
var Schema = mongoose.Schema;
var User = mongoose.model('user');

User.schema.plugin(function (schema, options) {
    require('./methods')(schema.methods);
    mongoose.rebuildModel('user', schema);
});