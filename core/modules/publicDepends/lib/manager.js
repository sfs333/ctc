var fs = require('fs');
var path = require('path');
var bytes = require('bytes');
var async = require('async');
var uuid = require('node-uuid');
var clc = require('cli-color');
var compressor = require('node-minify');
var moduleManager = require('moduleManager');
var config = moduleManager.use('config');
var objectStorage = moduleManager.use('objectStorage');
var log = moduleManager.use('log')(module);
var typeName = 'pDepends';
var typeStaticName = 'publicStatic';
var staticDependencies = ['app'];

var manager = function() {
    this.basePath =  '';
    this.publicPath = '';
    this.cachePath + '';
};

manager.prototype.run = function(app, callback) {

    this.basePath = app.get('basePath');
    this.publicPath = path.join(this.basePath, 'public');
    this.cachePath = path.join(this.publicPath, 'js/cache');

    callback(null, this);
};

manager.prototype.clearCache = function() {
    var cachePath = this.cachePath;
    fs.readdir(this.cachePath, function(err, files) {
        if (err) return console.error(err);

        files.forEach(function(file) {
            fs.unlink(path.join(cachePath, file), function (err) {
                if (err) return console.error(err);
            });
        });
    });
};

manager.prototype.getDependency = function(name) {
    return objectStorage.get(typeName, [name]) || {};
};

manager.prototype.getScripts = function(dependencies, absolutePath) {
    var currents = {};
    var files = [];
    var manager = this;
    var prefix = (absolutePath || false) ? this.publicPath : '';
    dependencies = staticDependencies.concat(dependencies);
    var addDepFiles = function(name, level) {
        if (currents[name]) return;

        var level = level || 1;
        currents[name] = true;
        var dep = manager.getDependency(name);

        if (!dep || !dep.dependencies || !dep.scripts) {
            console.error('No correct public dependence ' + name);
            console.log(dep);
            return;
        }

        var file;
        var logPrefix = '';
        for (var i = 1; i <= level; i++)
            logPrefix += '  ';

        if (config.get('optimizeJS:logs'))
            console.log(logPrefix + clc.cyan(name + ':'));

        if (dep.dependencies.length > 0) {
            for (var dKey in dep.dependencies)
                addDepFiles(dep.dependencies[dKey], (level + 1));
        }

        for (var key in dep.scripts) {
            file = path.join(prefix, dep.scripts[key] + '.js');
            if (config.get('optimizeJS:logs'))
                console.log(logPrefix + '  ' + clc.yellow(file));
            files.push(file);
        }

    };

    for (var name in dependencies)
        addDepFiles(dependencies[name]);
    return files;
};

manager.prototype.joinJS = function(dependencies, callback) {

    // var callbacks = [];
    var fileName = uuid.v4() + '.js';
    var outPutPath = path.join(this.cachePath, fileName);
    // var tempPath = this.cachePath;
    // var resultStr = '';

    //return callback(null, 'ok');
    new compressor.minify({
        compressor: 'no-compress',
        input: this.getScripts(dependencies, true),
        // tempPath: tempPath,
        output: outPutPath,
        // buffer: 5000 * 1024,

        sync: false,
        callback: function(err, data) {
            if (err) return callback(err);
            log.info('Compiled js static: ' + bytes(data.length));
            callback(err, fileName);
        }
    });
};

manager.prototype.getID = function(dependencies) {
    return dependencies.join('').toLowerCase();
};

manager.prototype.getJS = function(dependencies, callback) {
    var id = this.getID(dependencies);
    var storage = objectStorage.get(typeStaticName, id);

    if (storage)
        return callback(null, storage);

    this.joinJS(dependencies, function(err, fileName) {
        objectStorage.set(typeStaticName, id, fileName)
        callback(err, fileName);
    });
};

manager.prototype.addDependency = function(dep) {
    var storageDep;
    for (var name in dep) {
        storageDep = objectStorage.get(typeName, name);
        if (storageDep) {
            if (dep[name].scripts)
                storageDep.scripts = storageDep.scripts.concat(dep[name].scripts);
            if (dep[name].dependencies)
                storageDep.dependencies = storageDep.dependencies.concat(dep[name].dependencies);
        } else {
            objectStorage.set(typeName, name, {
                scripts: dep[name].scripts || [],
                dependencies: dep[name].dependencies || []
            });
        }
        if (dep[name].static)
            staticDependencies.push(name);
    }
};

module.exports = new manager();

