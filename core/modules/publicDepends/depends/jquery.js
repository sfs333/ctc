module.exports = function () {
    return {
        // jq: {scripts: ['js/jquery/jquery-2.1.1.min']},
        jq: {scripts: ['js/jquery/jquery.2.1.4']},
        jqBxSlider: {scripts: ['js/jquery/plugins/jquery.bxslider'],dependencies: ['jq']},
        jqCustomSelect: {scripts: ['js/jquery/plugins/jquery.customSelect'],dependencies: ['jq']},
        jqCookie: {scripts: ['js/jquery/plugins/jquery.cookie'],dependencies: ['jq']},
        jqTablesorter: {scripts: ['js/jquery/plugins/jquery.tablesorter.min', 'js/jquery/plugins/jquery.tablesorter.widgets'],dependencies: ['jq']},
        jqUiCore: {scripts: ['js/jquery/ui/jquery.ui.core'],dependencies: ['jq']},
        jqUiWidget: {scripts: ['js/jquery/ui/jquery.ui.widget'],dependencies: ['jq']},
        jqUiPosition: {scripts: ['js/jquery/ui/jquery.ui.position'],dependencies: ['jq']},
        jqUiEffect: {scripts: ['js/jquery/ui/jquery.ui.effect'],dependencies: ['jq']},
        jqUiDatepicker: {scripts: ['js/jquery/ui/jquery.ui.datepicker'],dependencies: ['jqUiCore']},
        jqUiMouse: {scripts: ['js/jquery/ui/jquery.ui.mouse'],dependencies: ['jqUiWidget']},
        jqUiEffectBlind: {scripts: ['js/jquery/ui/jquery.ui.effect-blind'],dependencies: ['jqUiEffect']},
        jqUiEffectBounce: {scripts: ['js/jquery/ui/jquery.ui.effect-bounce'],dependencies: ['jqUiEffect']},
        jqUiEffectClip: {scripts: ['js/jquery/ui/jquery.ui.effect-clip'],dependencies: ['jqUiEffect']},
        jqUiEffectDrop: {scripts: ['js/jquery/ui/jquery.ui.effect-drop'],dependencies: ['jqUiEffect']},
        jqUiEffectExplode: {scripts: ['js/jquery/ui/jquery.ui.effect-explode'],dependencies: ['jqUiEffect']},
        jqUiEffectFade: {scripts: ['js/jquery/ui/jquery.ui.effect-fade'],dependencies: ['jqUiEffect']},
        jqUiEffectFold: {scripts: ['js/jquery/ui/jquery.ui.effect-fold'],dependencies: ['jqUiEffect']},
        jqUiEffectHighlight: {scripts: ['js/jquery/ui/jquery.ui.effect-highlight'],dependencies: ['jqUiEffect']},
        jqUiEffectPulsate: {scripts: ['js/jquery/ui/jquery.ui.effect-pulsate'],dependencies: ['jqUiEffect']},
        jqUiEffectScale: {scripts: ['js/jquery/ui/jquery.ui.effect-scale'],dependencies: ['jqUiEffect']},
        jqUiEffectShake: {scripts: ['js/jquery/ui/jquery.ui.effect-shake'],dependencies: ['jqUiEffect']},
        jqUiEffectSlide: {scripts: ['js/jquery/ui/jquery.ui.effect-slide'],dependencies: ['jqUiEffect']},
        jqUiEffectTransfer: {scripts: ['js/jquery/ui/jquery.ui.effect-transfer'],dependencies: ['jqUiEffect']},
        jqUiAccordion: {scripts: ['js/jquery/ui/jquery.ui.accordion'],dependencies: ['jqUiCore', 'jqUiWidget']},
        jqUiButton: {scripts: ['js/jquery/ui/jquery.ui.button'],dependencies: ['jqUiCore', 'jqUiWidget']},
        jqUiProgressbar: {scripts: ['js/jquery/ui/jquery.ui.progressbar'],dependencies: ['jqUiCore', 'jqUiWidget']},
        jqUiTabs: {scripts: ['js/jquery/ui/jquery.ui.tabs'],dependencies: ['jqUiCore', 'jqUiWidget']},
        jqUiMenu: {scripts: ['js/jquery/ui/jquery.ui.menu'],dependencies: ['jqUiCore', 'jqUiWidget', 'jqUiPosition']},
        jqUiTooltip: {scripts: ['js/jquery/ui/jquery.ui.tooltip'],dependencies: ['jqUiCore', 'jqUiWidget', 'jqUiPosition']},
        jqUiAutocomplete: {
            scripts: ['js/jquery/ui/jquery.ui.autocomplete'],
            dependencies: ['jqUiCore', 'jqUiWidget', 'jqUiPosition', 'jqUiMenu']
        },
        jqUiDraggable: {scripts: ['js/jquery/ui/jquery.ui.draggable'],dependencies: ['jqUiCore', 'jqUiMouse', 'jqUiWidget']},
        jqUiResizable: {scripts: ['js/jquery/ui/jquery.ui.resizable'],dependencies: ['jqUiCore', 'jqUiMouse', 'jqUiWidget']},
        jqUiSelectable: {scripts: ['js/jquery/ui/jquery.ui.selectable'],dependencies: ['jqUiCore', 'jqUiMouse', 'jqUiWidget']},
        jqUiSlider: {scripts: ['js/jquery/ui/jquery.ui.slider'],dependencies: ['jqUiCore', 'jqUiMouse', 'jqUiWidget']},
        jqUiSortable: {scripts: ['js/jquery/ui/jquery.ui.sortable'],dependencies: ['jqUiCore', 'jqUiMouse', 'jqUiWidget']},
        jqUiSpinner: {scripts: ['js/jquery/ui/jquery.ui.spinner'],dependencies: ['jqUiCore', 'jqUiWidget', 'jqUiButton']},
        jqUiDroppable: {
            scripts: ['js/jquery/ui/jquery.ui.droppable'],
            dependencies: ['jqUiCore', 'jqUiWidget', 'jqUiMouse', 'jqUiDraggable']
        },
        jqUiDialog: {
            scripts: ['js/jquery/ui/jquery.ui.dialog'],
            dependencies: ['jqUiCore', 'jqUiWidget', 'jqUiButton', 'jqUiDraggable', 'jqUiMouse', 'jqUiPosition', 'jqUiResizable']
        }
    }
};
