module.exports = function () {
    return {
        app: {
            scripts: [
                'js/socket.io',
                'js/app/index',
                'js/app/events',
                'js/app/lib',
                'js/app/tools',
                'js/io',
                'js/app/js-values',
                'js/app/actions',
            ],
            dependencies: ['bootstrap'],
        },
        bootstrap: {
            scripts: ['js/bootstrap/bootstrap.min'],
            dependencies: ['jq'],
            // static: true
        },
        async: {
            scripts: ['js/async'],
            dependencies: ['app', 'jq'],
            static: true
        }
    }
};
