var fs = require('fs');
var path = require('path');
var bytes = require('bytes');
var async = require('async');
var uuid = require('node-uuid');
var config = require('lib/config');
var manager = require('./lib/manager');

manager.addDependency(require('./depends/jquery')());
manager.addDependency(require('./depends/plugins')());
manager.addDependency(require('./depends/app')());

module.exports.run = function(app, callback) {

    app.use(require('./middleware/loadScripts'));
    manager.run(app, function(err, manager) {
        if (err) return callback(err);
        manager.clearCache();
        callback(null);
    });
};

module.exports.getDependency = manager.getDependency;
module.exports.addDependency = manager.addDependency;
module.exports.getScripts = function(depends) {
    return manager.getScripts(depends);
};
module.exports.getJS = function(depends, callback) {
    return manager.getJS(depends, callback);
};

