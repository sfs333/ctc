var moduleManager = require('moduleManager');
var views = moduleManager.use('views');
var mongoose = require('mongoose');
var async = require('async');

module.exports.get = function (req, res, next) {
    var view = views.get('exMenu');
    view.render('exMenuD1',
        { handler: 'table' },
        function (context, cb) {
            cb(null, {
                items: [
                    {title: 'Users', link: '724check/user-list'},
                    {title: 'Pages', link: '724check/pages-list'},
                    {title: 'Sessions', link: '724check/ses-list'}
                ],
                count: 3
            });
        },
        function (err, html) {
            if (err) return next(err);
            res.render('page', {
                title: 'Checking',
                content: html
            });
        }
    );
};