var moduleManager = require('moduleManager');
var views = moduleManager.use('views');
var mongoose = require('mongoose');
var async = require('async');

module.exports.get = function (req, res, next) {

    var view = views.get('userList');
    view.render('userListD1',
        {
            handler: 'table'
        },
        function (context, cb) {
            var User = mongoose.model('user');
            async.parallel([
                function(cb) {
                    User.count(cb);
                },
                function(cb) {
                    User
                        .find({})
                        .limit(view.pagination.count)
                        .skip((context.page - 1) * view.pagination.count)
                        //.sort('-occupation')
                        //.select('name occupation')
                        .exec(cb);
                }
            ], function(err, results) {
                if (err) return cb(err);

                cb(null, {
                    items: results[1],
                    count: results[0]
                    /** other options **/
                });
            });
        },
        function (err, html) {
            if (err) return next(err);
            res.render('page', {
                title: 'Users',
                content: html
            });
        }
    );
};