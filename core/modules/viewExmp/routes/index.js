module.exports = function(app) {
    app.get('/724check/user-list', require('./list').get);
    app.get('/724check/pages-list', require('./pages').get);
    app.get('/724check/ses-list', require('./sessions').get);
    app.get('/724check', require('./menu').get);
}