var moduleManager = require('moduleManager');
var views = moduleManager.use('views');
var mongoose = require('mongoose');
var async = require('async');

module.exports.get = function (req, res, next) {
    var view = views.get('pagesList');
    view.render('pagesListD1',
        {
            handler: 'table'
        },
        function (context, cb) {
            var Page = mongoose.model('Page');
            async.parallel([
                function(cb) {
                    Page.count(cb);
                },
                function(cb) {
                    Page
                        .find({})
                        .limit(view.pagination.count)
                        .skip((context.page - 1) * view.pagination.count)
                        .exec(cb);
                }
            ], function(err, results) {
                if (err) return cb(err);

                cb(null, {
                    items: results[1],
                    count: results[0]
                    /** other options **/
                });
            });
        },
        function (err, html) {
            if (err) return next(err);
            res.render('page', {
                title: 'Pages',
                content: html
            });
        }
    );


};