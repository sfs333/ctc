var moduleManager = require('moduleManager');
var views = moduleManager.use('views');
var mongoose = require('mongoose');
var async = require('async');

module.exports.get = function (req, res, next) {
    var view = views.get('sesList');
    view.render('sesListD1',
        {
            handler: 'table'
        },
        function (context, cb) {
            var User = mongoose.model('user');
            async.parallel([
                function(cb) {
                    User.count(cb);
                },
                function(cb) {
                    User.aggregate([
                        {$unwind: "$broadcastSession"},
                        {$unwind: "$profile"},
                        {$skip: ((context.page - 1) * view.pagination.count)},
                        {$limit : view.pagination.count},
                        {$project: {
                            name:'$profile.chatName',
                            picture: '$profile.picture',
                            online: '$broadcastSession.isActive',
                            email: '$profile.email',
                            sex: '$profile.sex',
                            bday: '$profile.bday'
                        }}
                    ], cb);
                }
            ], function(err, results) {
                if (err) return cb(err);

                cb(null, {
                    items: results[1],
                    count: results[0]
                    /** other options **/
                });
            });
        },
        function (err, html) {
            if (err) return next(err);
            res.render('page', {
                title: 'Sessions',
                content: html
            });
        }
    );
};