var async = require('async');
module.exports = function(opt) {

    return function (context, cb) {
        var Model = require('mongoose').model(opt.modelName);
        var self = this;
        async.parallel([
            function(cb) {
                Model.count(cb);
            },
            function(cb) {
                var q = Model
                        .find({})
                        .limit(self.pagination.count)
                        .skip((context.page - 1) * self.pagination.count);

                    if (opt.sort) {
                        var sortInf = {};
                        sortInf[opt.sort] = -1;
                        q.sort(sortInf);
                    }

                    q.exec(cb);
            }
        ], function(err, results) {
            if (err) return cb(err);
            cb(null, {
                items: results[1],
                count: results[0]
                /** other options **/
            });
        });
    }
}
