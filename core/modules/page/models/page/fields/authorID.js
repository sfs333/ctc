var moduleManager = require('moduleManager');
module.exports = {
    type: moduleManager.use('mongoose').Schema.Types.ObjectId,
    required: true
}