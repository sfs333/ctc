module.exports = {
    type: Date,
    default: Date.now,
    required: true
}