var moduleManager = require('moduleManager');
var log = moduleManager.use('log')(module);
var mongoose = require('mongoose');
var Page = mongoose.model('Page');

module.exports.get = function (req, res, next) {
    if (!req.params.id || !mongoose.isObjectId(req.params.id)) {
        res.render('page/edit', {
            title: 'Create Page',
            page: false
        });
    } else {
        Page.findById(req.params.id, function(err, page) {
            if (err) return next(err);
            if (!page) return next(new Error('No find page'));

            res.render('page/edit', {
                title: page ? page.get('title') : 'CREATE NEW PAGE',
                page: page
            });
        });
    }

};