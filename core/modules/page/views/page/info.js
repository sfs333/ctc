module.exports = {
    pagination: {
        count: 15
    },
    fields: {
        title:{
            title: 'Title',
            type: 'string',
            params: {}
        },
        public:{
            title: 'Publish',
            type: 'boolean'
        },
        created:{
            title: 'Created',
            type: 'date',
            params: {
                format: 'DD.MM.YYYY h:mm:ss a'
            }
        },
        actions: {
            title: 'Actions',
            type: 'markup',
            params: {
                template: 'page/actions'
            }
        }
    }
};