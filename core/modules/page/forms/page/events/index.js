module.exports = function(mf) {
    mf.formType.on('build', function(params, callback) {
        this.on('validate', function(callback) {
            var self = this;
            this.socket.request.loadUser(function(err, user) {
                if (err) return callback(err);

                if (!user.isAdmin)
                    return callback(new Error('Access denied'));

                self.mf.document.set('authorID', user.get('id'))
                callback();
            });
        });
        callback();
    });

    mf.formType.on('submit', function(callback) {
        this.response.actions.redirect = {url: 'admin/content'};
        callback();
    });
}