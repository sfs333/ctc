module.exports = function () {
    this.name = 'admin-page-form';
    this.buttons = {
        submit: {title: 'save'},
        cancel: false
    }
    this.getFields = function () {
        return {
            title: {
                type: 'text',
                title: 'Title',
                placeholder: 'Enter title',
                required: true
            },
            body: {
                type: 'editor',
                title: 'Body',
                maxSize: 5
            },
            public: {
                type: 'checkbox',
                title: 'Publish'
            }
        }
    }
}