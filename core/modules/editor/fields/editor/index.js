var modules = require('modules');
var path = require('path');
var fs = require('fs.extra');
var async = require('async');
var Textarea = modules.use('form', '/fieldsFactory/fields/textarea');
var util = require('util');
var log = modules.use('log')(module);
var mime = require('mime-types');
var uuid = require('node-uuid');
var app = require('app');

var Editor = function(info, form) {
    var field = this;
    info.type = 'textarea';
    this.construct(info, form);
    this.addClass('summernote-editor');
    this.fileDir = info.fileDir || 'editor';
    this.maxSize = info.maxSize || 2;
    this.minSize = info.minSize || false;
    this.allowTypes = info.allowTypes || false;
}

util.inherits(Editor, Textarea);
var proto = Editor.prototype;

proto.upload = function(stream, info, cb) {
    log.info('Start uploading ' + info.name + '...');
    var field = this,
     i = 0,
     uploadedSize = 0,
     ext = mime.extension(info.type),
     name = util.format('%s.%s', uuid.v4(), ext),
     dir = path.join(app.get('publicPath'), 'files', this.fileDir),
     publishPath = path.join('files', this.fileDir, name),
     filePath = path.join(dir, name);

    async.waterfall([
        function(cb) {
            if (field.allowTypes && (util.isArray(field.allowTypes)) && (field.allowTypes.indexOf(ext) == -1))
                return cb(new Error(util.format('Allow types only: %s', field.allowTypes.join(', '))));

            return cb();
        },
        function(cb) {
            fs.exists(dir, function(exist) {
                if (exist) return cb();
                fs.mkdir(dir, cb);
            });
        },
        function(cb) {
            var writeStream = fs.createWriteStream(filePath);
            stream.pipe(writeStream);
            writeStream.on('drain', function() {
                i++;
                uploadedSize = (i * writeStream._writableState.highWaterMark) / 1048576;
                if (uploadedSize > field.maxSize) {
                    writeStream.end();
                    stream.unpipe();
                    return cb(new Error(util.format('File should be less than %s MB', field.maxSize)));
                }
            });

            stream.on('end', function() {
                if ((typeof field.minSize === 'number') && (uploadedSize < field.minSize))
                    return cb(new Error(util.format('File should be more than %s MB', field.minSize)));

                cb(null, {
                    path: publishPath,
                    name: name,
                    message: util.format('File %s saved in temp',  info.name)
                });
            });
        }
    ], function(err, info) {
        if (err) {
            cb({status: false, message: err.message});
            return log.error(err);
        }
        cb({
            status: true,
            message: info.message,
            path: info.path,
            name: info.name
        });
    });
}

module.exports = Editor;