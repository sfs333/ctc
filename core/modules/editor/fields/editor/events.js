var path = require('path');
var async = require('async');
var fs = require('fs.extra');
var lib = require('./../../lib');

module.exports = function(field, form) {
    //Upload
    form.on('submit', function (cb) {
        if (field.files.length === 0)
            return cb();

        var inputDir = path.join('files', field.fileDir),
            realInputDir = path.join(lib.systemPath, inputDir);

        async.waterfall([
            function(cb) {
                fs.exists(realInputDir, function(exist) {
                    if (exist) return cb();
                    fs.mkdir(realInputDir, cb);
                });
            },
            function(cb) {
                if (field.multiple)
                    return cb();
                if (!field.value[0])
                    return cb();

                field.removeFile.apply(field, [field.value[0], cb]);
            },
            function(cb) {
                var workers = [];
                var worker = function(file, id) {
                    workers.push(function(cb) {
                        field.save(file, id, cb);
                    });
                }
                field.files.forEach(function(file, id) {
                    worker(file, id);
                });
                async.series(workers, cb);
            }], cb);
    });
    //Remove Once
    form.on('submit', function (cb) {
        if (!field.input || (field.input.length > 0) || !field.value[0])
            return cb();

        field.removeFile(field.value[0], cb);
    });
    //Remove Multiple
    form.on('submit', function (cb) {

        if (!field.input || !field.input.delete || (field.input.delete.length === 0))
            return cb();

        var workers = [];
        var worker = function(file) {
            workers.push(function(cb) {
                if (typeof field.value.id === 'function') {
                    var info = field.value.id(file.id);
                    if (!info)
                        return cb(new Error('File by id not find'));
                    return field.removeFile(info, cb);
                }
                return cb(new Error('Not support remove by field name now'));
            });
        }
        field.input.delete.forEach(worker);
        async.parallel(workers, cb);
    });
}