var modules = require('modules');
var publicDepends = modules.use('publicDepends');
var fieldsFactory = modules.use('formFieldFactory');

module.exports.run = function(app, cb) {
    app.use(require('./middleware/addJs'));
    fieldsFactory.register('editor', require('./fields/editor'));
    cb();
}

publicDepends.addDependency({
    editor: {
        scripts: [
            'js/editor/codemiror/codemirror',
            'js/editor/codemiror/xml',
            'js/editor/codemiror/formatting',
            'js/editor/summernote',
            'js/editor/summernote-cleaner',
            // 'js/editor/summernote-input',
            // 'js/editor/summernote-media',
            'js/editor/index',
            'js/editor/form',
            'js/editor/config',
            // 'js/editor/events',
            // 'js/editor/upload'
        ],
        dependencies: ['form', 'jq', 'socketStream']
    }
});