var modules = require('modules');
var user = require('mongoose').model('user');

module.exports = function(req, res, next) {
    if (!req.user || !req.user.isAdmin)
        return next();


    res.dependency('editor');
    next();
}