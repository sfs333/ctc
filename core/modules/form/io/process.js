var moduleManager = require('moduleManager');
var async = require('async');
var clc = require('cli-color');
var log = moduleManager.use('log')(module);
var objectStorage = moduleManager.use('objectStorage');
var lib = require('./../lib');

module.exports = function (socket) {
    socket.on('formProcess', function (data, frontCallback) {
        var form = null;
        if (!data.id || !(form = lib.getFormItem(data.id))) {
            var error = 'Don`t correct id form';
            log.error(new Error(error));

            return frontCallback({message: error, status: 500}, null);
        }
        form.clearTempData();
        var sessionManager = socket.request.sessionManager;
        form.socket = socket;
        async.series([
            function (callback) {
                form.saveData(data, callback);
            },
            function (callback) {
                sessionManager.get(function (err, session) {
                    if (err) {
                        callback(err);
                        return;
                    }
                    form.session = session;
                    callback(err);
                });
            },
            function (callback) {
                form.validate(callback);
            },
            function (callback) {
                if (data.submit)
                    form.submit(callback);
                else callback(null);
            },
            function (callback) {
               if (data.submit && form.autoSaveSession)
                    sessionManager.set(form.session, callback);
               else
                   callback();
            }
        ], function (err, results) {
            if (err) {
                form.response.status = 403;
                err.message = err.message || 'Error validate in form';
                log.warn(err.message);

                form.response.actions.formErrorValidate = {
                    id: form.id,
                    type: form.name,
                    message: err.message
                }
                if (typeof err.field == 'string')
                    form.response.actions.formErrorValidate.field = err.field;

            }
            frontCallback(null, form.response);
//                form.destroy();
            log.info('form ' + form.id + clc.green(' ' + (form.isSubmit ? 'submit' : 'validate') + ' process'));
        });
    });
}