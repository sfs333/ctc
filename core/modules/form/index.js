var moduleManager = require('moduleManager');
var publicDepends = moduleManager.use('publicDepends');

moduleManager.register('formFieldFactory', 'modules/form/fieldsFactory');
moduleManager.register('formFieldField', 'modules/form/fieldsFactory/fields/field');
moduleManager.register('formType', 'modules/form/type');
var lib = require('./lib');
require('./parts');
module.exports = lib;

module.exports.run = function(app, callback) {
    require('./io/index')(app.get('io'));
    callback(null);
}

publicDepends.addDependency({
    form: {
        scripts: ['js/form/index'],
        dependencies: ['app', 'jq'],
        static: true
    }
});