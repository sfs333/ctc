var util = require('util');
var Field = require('./field');

var CheckboxGroup = function(info, form) {
    this.construct(info, form);
    this.items = info.items || {};
}

util.inherits(CheckboxGroup, Field);
module.exports = CheckboxGroup;