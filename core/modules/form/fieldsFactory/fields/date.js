var util = require('util');
var Field = require('./field');
var moduleManager = require('moduleManager');
var ejsTools = moduleManager.use('ejsTools');
var FormDate = function(params, form) {
    this.construct(params, form);
}

util.inherits(FormDate, Field);
var proto = FormDate.prototype;
var protoRender = proto.render;
proto.render = function(cb) {
    if (!this.value)
        this.value = new Date();
    var value = this.value.getFullYear() + '-'
    + ('0' + (this.value.getMonth()+1)).slice(-2) + '-'
    + ('0' + this.value.getDate()).slice(-2);

    this.setDefault(value);
    protoRender.apply(this, [cb]);
}

//this.fieldClass = this.fieldClass ? (this.fieldClass + ' ' + this.type) : this.type;
//ejsTools.read({
//    path: this.template,
//    absolutePath: false,
//    theme: this.form.theme,
//    variables: {
//        id: (this.form.name + '-' + this.name + '-field'),
//        field: this,
//        defaultValue: defaultValue
//    }
//}, callback);

module.exports = FormDate;




