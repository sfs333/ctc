var util = require('util');
var Field = require('./field');

var Hidden = function(params, form) {
    this.construct(params, form);
}

util.inherits(Hidden, Field);
module.exports = Hidden;