var util = require('util');
var Field = require('./field');

var Pass = function(params, form) {
    this.construct(params, form);
}

util.inherits(Pass, Field);
module.exports = Pass;