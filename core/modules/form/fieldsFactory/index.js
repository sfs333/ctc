var moduleManager = require('moduleManager');
var Field = require('./fields/field');
var log = moduleManager.use('log')(module);

var FieldsFactory = function() {
    this.types = {};
};

FieldsFactory.prototype.firstUp = function(str) {
    return str.charAt(0).toUpperCase() + str.substr(1);
};

FieldsFactory.prototype.register = function(type, field) {
    if (typeof type != 'string')
        return log.error('No correct type');
    if (typeof field != 'function' || (field instanceof Field))
        return log.error('No correct field');

    this.types[type] = field;
};

FieldsFactory.prototype.create = function(info, form) {
    if (!info.type || !this.types[info.type])
        return log.error(new Error('Not correct type in field info - ' + info.type));

    var field =  new this.types[info.type](info, form);

    for (var key in info) {
        if (!field[key] && (typeof field[key] !== 'boolean'))
            log.warn('Not registered field info - ' + key);
    }

    return field;
}

var factory =  new FieldsFactory();
factory.register('checkbox', require('./fields/checkbox'));
factory.register('date', require('./fields/date'));
factory.register('checkboxGroup', require('./fields/checkboxGroup'));
factory.register('radios', require('./fields/radios'));
factory.register('email', require('./fields/email'));
factory.register('pass', require('./fields/pass'));
factory.register('select', require('./fields/select'));
factory.register('text', require('./fields/text'));
factory.register('hidden', require('./fields/hidden'));
factory.register('number', require('./fields/number'));
factory.register('textarea', require('./fields/textarea'));
factory.register('markup', require('./fields/markup'));

module.exports = factory;