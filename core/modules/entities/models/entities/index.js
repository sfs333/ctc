var modules = require('modules');
var log = modules.use('log')(module);
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var name = 'Entities';
var opts = {};
var info = {
        name: require('./fields/name'),
        title: require('./fields/title'),
        description: require('./fields/description'),
        created: require('./fields/created'),
        fields: [new Schema(require('./subFields'), opts)]
}

var schema = new Schema(info, opts);
mongoose.model(name, schema);