var modules = require('modules');
var mongoose = require('mongoose');
var util = require('util');
var async = require('async');
var lib = require('./index');

var fields = require('./../fields');
var EntitiesClass = mongoose.model('Entities');


var Entities = function(doc, cb) {
    this.info = '';
    this.name = doc.get('name');
    this.fields = {};
    this.doc = doc;

    this.build(cb);
}

var proto = Entities.prototype;

proto.build = function(cb) {
    if (!this.getModel())
        this.makeModel();

    cb(null, this);
}

proto.makeModel = function() {
    //here building model
}

proto.save = function(cb) {
    this.doc.save(cb);
}

proto.getModel = function() {
    return mongoose.getModel(this.name);
}

proto.fieldIsExist = function(info, cb) {
    EntitiesClass.findOne({_id: this.doc.get('_id'), 'fields.name' : info.name}, {_id: 1}, function(err, res) {
        if (err) return cb(err);

        cb(null, (Boolean(res)));
    });
}

proto.attachField = function(type, info, opt, cb) {

    var field = fields.get(type);
    if (!field)
        return cb(new Error(util.format('Not exist type field for entities.', type)));
    var self = this;
    var autoSave = Boolean(opt.save);
    delete opt.save;
    async.waterfall([
       function(cb) {
           self.fieldIsExist(info, cb);
       },
        function(exist, cb) {
            if (exist)
                return cb(new Error(util.format('Field %s already exist', info.name)));

            self.doc.fields.push({
                 name: info.name,
                 title: info.title || '',
                 description: info.description || '',
                 weight: info.weight || 1,
                 opt: opt
            });

            if (autoSave)
                return self.save(cb);

            cb();
        }
    ], cb);
}

module.exports = Entities;