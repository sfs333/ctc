var moduleManager = require('moduleManager');
var log = moduleManager.use('log')(module);
var ejsLocals = require('ejs-locals');
var util = require('util');

var templatePath = 'views';
module.exports = function(view, opt, variables, cb) {
    ejsLocals(view.getTemplatePath(util.format('%s/handlers/table', templatePath), opt.theme), variables, cb);
};