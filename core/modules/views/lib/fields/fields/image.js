var util = require('util');
var Field = require('./field');

var Image = function(params, view) {
    this.construct(params, view);
}
util.inherits(Image, Field);
module.exports = Image;