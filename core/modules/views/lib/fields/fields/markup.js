var util = require('util');
var Field = require('./field');
var moduleManager = require('moduleManager');
var ejsTools = moduleManager.use('ejsTools');

var Markup = function(params, view) {
    this.construct(params, view);
    if (!this.params.template)
        this.params.template = false;
}
util.inherits(Markup, Field);
var proto = Markup.prototype;
var parentRender = proto.render;

proto.render = function(cb) {
    if (!this.params.template) {
        //this.set('');
        return parentRender.apply(this, [cb]);
    }
    ejsTools.read({
        path: this.params.template,
        absolutePath: false,
        theme: this.theme,
        variables: {
            id: this.fields.id,
            field: this,
            fields: this.fields,
            view: this.view
        }
    }, cb);
}

module.exports = Markup;