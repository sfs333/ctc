var util = require('util');
var Field = require('./field');

var Link = function(params, view) {
    this.construct(params, view);
    this.params.title = this.params.title || 'link';

}
util.inherits(Link, Field);
var proto = Link.prototype;
var render = proto.render;

proto.render = function() {
    if ((this.params.title !== 'id') && this.fields[this.params.title])
        this.params.title = this.fields[this.params.title].field.get();

    render.apply(this, arguments);
}

module.exports = Link;