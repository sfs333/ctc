var util = require('util');
var Field = require('./field');
var moment = require('moment');

var Date = function(params, view) {
    this.construct(params, view);
    if (!this.params.format)
        this.params.format = 'L';
}
util.inherits(Date, Field);
var proto = Date.prototype;
var parentGetter = proto.get;
proto.get = function() {
    return moment(parentGetter.apply(this) || undefined).format(this.params.format);
}

module.exports = Date;