var moduleManager = require('moduleManager');
//var mongoose = require('mongoose');
var View = require('./view');
var log = moduleManager.use('log')(module);
var Views = {
    storage: {},
    handlers: {
        menuButtons: require('./handlers/menuButtons'),
        simple: require('./handlers/simple'),
        table: require('./handlers/table')
    }
};

var empCallback = function(err) {
    if(err)
        log.error(err);

    log.warn('Now use default views callback');
};

Views.get = function(name) {
    return this.storage[name] || null;
};
/**
 *  Register new model
 * @param model name
 * @param [info]
 * @param callback(err, view)
 */
Views.add = function(name, pinfo, cb) {
    //var model = false;
    var info = ((arguments.length > 1) && (typeof pinfo === 'object')) ? pinfo : {};
    if (arguments.length === 2)
        cb = arguments[1];
    if (typeof cb !== 'function')
        cb = empCallback;
    //if ((typeof modelName !== 'string') || !(model = mongoose.getModel(modelName)))
    //    return cb(new Error('Not correct first param (model name) or model not exist'));

    //console.log('model name ' + modelName);
    //console.log('info ');
    //console.log(info);

    //new View(model, info, function(err, view) {
    new View(name, info, Views, function(err, view) {
        if (err) return cb(err);

        Views.storage[name] = view;

        cb(err, view);
    });
};
module.exports = Views;