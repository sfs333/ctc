var moduleManager = require('moduleManager');
var objectStorage = moduleManager.use('objectStorage');
var publicDepends = moduleManager.use('publicDepends');

module.exports.run = function (app, callback) {
    require('./io')(app.get('io'));
    callback(null);
}

var clientCallback = function (socket, action, data, callback, maxPeriod) {
    maxPeriod = maxPeriod || 30000;
    var id = objectStorage.set('clientCallback', false, callback);
    socket.emit('clientCallback', {
        id: id,
        params: data,
        action: action
    });

    setTimeout(function () {
        objectStorage.remove('clientCallback', id);
    }, maxPeriod);
}

publicDepends.addDependency({ioClientCb: {scripts: ['js/ioClientCallback/io'], static: true}});

module.exports.clientCallback = clientCallback;