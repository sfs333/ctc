var moduleManager = require('moduleManager');
var objectStorage = moduleManager.use('objectStorage');

module.exports = function(io) {
    io.on('connection', function (socket) {
        socket.on('clientCallback', function(data) {
            var err = data.error || null;
            var response = data.response || {};
            var callback = objectStorage.get('clientCallback', data.id);
            if (typeof callback == 'function')
                callback(err, response);
            else
                console.error('Not find callback from owner!');
            objectStorage.remove('clientCallback', response.id);
        });
    });
}