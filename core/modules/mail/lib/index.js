var modules = require('modules');
var config = modules.use('config').get('mail');
var transports = require('./transports');
var util = require('util');

var mail = module.exports = {};

mail.send = function(info, cb) {
    var transporter;
    if (!config.transport || (typeof transports[config.transport] !== 'function'))
        return cb(new Error(util.format('Transport %s not find', config.transport)));

    if (!(transporter = transports[config.transport](config.options || {})))
        return cb(new Error(util.format('Transport %s is failed', config.transport)));

        if (typeof info != 'object')
            return cb(new Error('Info must be object type'));

        if (info.to.length < 3)
            return cb(new Error('Not exist field "to" for mail confirmation, it should be mail'));

        if (config.from)
            info.from = config.from;

        transporter.sendMail(info, cb);
}

//setTimeout(function() {
//    mail.send({
//        from: config.from,
//        to: 'ruslan.sfs@gmail.com',
//        subject: 'Hello ruslan',
//        text: 'plain text',
//        html: 'and <b>Hello html text</b>'
//    },
//    function() {
//        console.log("SENDED");
//        console.log(arguments);
//    });
//}, 100);

