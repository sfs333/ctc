var modules = require('modules');
var lib = require('./lib');
var publicDepends = modules.use('publicDepends');
var fieldsFactory = modules.use('formFieldFactory');
module.exports = lib;

module.exports.run = function(app, cb) {
    fieldsFactory.register('file', require('./fields/file'));
    fieldsFactory.register('image', require('./fields/image'));
    require('./io')(app.get('io'));
    require('./treeViews');
    cb();
}

publicDepends.addDependency({
    socketStream: {
        scripts: ['js/socket.io-stream'],
        dependencies: ['app']
    },
    form: {
        scripts: ['js/formFile/index', 'js/formFile/removeValueItem'],
        dependencies: ['socketStream']
    }
});