var modules = require('modules');
var path = require('path');
var validator = require('validator');
var fs = require('fs.extra');
var bytes = require('bytes');
var async = require('async');
var Field = modules.use('formFieldField');
var util = require('util');
var log = modules.use('log')(module);
var uuid = require('node-uuid');
var mime = require('mime-types');
var lib = require('./../../lib');
var treeView = modules.use('treeView');
var merge = require('util-merge');


var File = function(info, form) {
    this.construct(info, form);
};

util.inherits(File, Field);
var proto = File.prototype;
var parentRender = proto.render;
var parentPrepareRemove = proto.prepareRemove;
var fieldConstruct = proto.construct;
proto.construct = function(info, form) {
    var field = this;
    fieldConstruct.apply(this, [info, form]);
    this.maxSize = info.maxSize || 2;
    this.minSize = info.minSize || false;
    this.fileDir = info.fileDir || '';
    this.fileName = info.fileName || false;
    this.allowTypes = info.allowTypes || false;
    this.saveOriginalName = info.saveOriginalName || false;
    this.files = [];
    this.removes = [];
    this.inputDir = path.join('files', this.fileDir);
    this.realInputDir = path.join(lib.systemPath, this.inputDir);
    this.previewsViewName = 'filePreviews';
    this.previews = '';
    require('./events')(field, form);
};
proto.prepareRemove = function(info, cb) {
    var self = this;
    if (self.files[info.id]) {
        async.series([
            function(cb) {
                self.removeFile(self.files[info.id], cb)
            },
            function(cb) {
                delete self.files[info.id];
                cb();
            }
        ], cb);
        return;
    }

    parentPrepareRemove.apply(self, arguments);
};
proto.getViewItem = function(info, cb) {
    var view = treeView.get(this.previewsViewName).getView();
    view.renderItem(info, {theme: 'admin', line: true}, cb);
};
proto.viewData = function(items) {
    items.forEach(function(item) {
        item.publicPath = {value: item.publicPath, params :{ title: item.name }};
    });
    return items;
};
proto.render = function(cb) {
    var self = this;

    if (!this.value)
        this.value = [];

    var items = (typeof this.value.toObject === 'function') ? this.viewData(this.value.toObject()) : this.value;
    modules.use('treeView')
        .get(self.previewsViewName)
        .render({
            theme: 'admin',
            form: this.form.id,
            field: this.name,
            //colCount: 4,
            data: items,
            sort: true
        }, function(err, html) {
            self.previews = html;
            parentRender.apply(self, [cb]);
        });
};

proto.get = function() {
    return this.multiple ? (this.input || []) : ((this.input && this.input[0]) ? this.input[0] : null);
};

proto.set = function(inf) {
    if (typeof inf === 'object')
        this.input = util.isArray(inf) ? inf : [inf];
};

proto.addFile = function(inf) {
    if (this.multiple)
        return this.input.add.push(inf);
    else
        return this.set(inf);
};

proto.changeFile = function(id, inf) {
    if (validator.isMongoId(id)) {
        var place = this.input.edit, removed;
        place.forEach(function(item, key) {
            if (item.id === id)
                return removed = place.splice(key, 1, merge(item, inf));
        });
        if (!removed)
            place.push(merge({id: id}, inf));
        return;
    }

    if (this.files[id])
        return this.files.splice(id, 1, merge(this.files[id], inf));
}

proto.save = function(file, id, cb) {
    var field = this,
        name = field.fileName ? util.format('%s.%s', field.fileName, file.ext) : file.name,
        savePath = path.join(field.inputDir, name),
        realPathFile = path.join(lib.systemPath, savePath);

    async.waterfall([
        function(cb) {
            fs.exists(realPathFile, function(exists) {
                cb(null, exists);
            });
        },
        function(exists, cb) {
            if (exists)
                fs.unlink(realPathFile, arguments);
            cb();
        },
        function(cb) {
            fs.move(file.path, realPathFile, cb);
        },
        function(cb) {
            field.addFile({
                path: realPathFile,
                publicPath: savePath,
                type: file.type || 'txt',
                size: file.size,
                weight: file.weight,
                name: file.name
            });
            cb();
        }
    ], cb);

};

proto.removeFile = function(info, cb) {
    if (typeof cb !== 'function')
        cb = function(err) {if (err) return log.error(err) }
    if (typeof info === 'object') {
        if (typeof info.path !== 'string') {
            console.error('path is', info);
            return cb();
            // return cb(new Error('Incorrect info.path'));
        }

        return async.waterfall([
            function(cb) {
                fs.exists(info.path, function(exist) {
                    cb(null, exist);
                });
            },
            function(exist, cb) {
                if (!exist) return cb();
                fs.unlink(info.path, cb);
            },
        ], cb);
    }

    cb(new Error('Not info field for remove'));
};

proto.clearTemp = function(callback) {
    if (this.tempFile && this.tempFile.path)
        fs.unlink(this.tempFile.path, callback);
};

proto.upload = function(stream, info, cb) {
    log.info('Start uploading ' + info.name + '...');
    var field = this;
    var i = 0;
    var uploadedSize = 0;
    var ext = mime.extension(info.type);
    var writeStream;
    var tempFileName;
    var file;
    var n;

    var response = function(status, message) {
        log[status ? 'info' : 'warn'](message);
        var info = {
            status: status,
            message: message,
            item: {}
        };
        if (!status) return cb(info);
        field.getViewItem(
            {
                _id: n,
                publicPath: {
                    //value: '#',
                    value: path.join('files', 'temp', tempFileName),
                    params :{ title: file.name }
                },
                size: file.size,
                name: file.name,
                date: null,
                weight: String(file.weight),
                type: file.type
            }, function(err, item) {
            info.item = item;
            cb(info);
        });
    }

    if (field.allowTypes && (util.isArray(field.allowTypes)) && (field.allowTypes.indexOf(ext) == -1))
        return response(false, 'Allow types only: ' + field.allowTypes.join(', '));

    //TODO return clear
    //this.clearTemp();

    tempFileName = field.saveOriginalName ? info.name : (uuid.v4() + '.' + ext);
    file = {
        name: tempFileName,
        ext: ext,
        type: String(info.type),
        path: path.join(lib.tempPath, tempFileName),
        size: Number(info.size),
        weight: 0
    };
    writeStream = fs.createWriteStream(file.path);
    stream.pipe(writeStream);
    if (field.multiple) {
        n = field.files.push(file) - 1;
    } else {
        n = 0;
        if (field.files[0])
            field.removeFile(field.files[0]);
        field.files[0] = file;
    }
    writeStream.on('drain', function() {
        i++;
        uploadedSize = (i * writeStream._writableState.highWaterMark) / 1048576;
        if (uploadedSize > field.maxSize) {
            writeStream.end();
            stream.unpipe();
            return response(false, 'File should be less than ' + field.maxSize + 'MB');
        }
    });

    stream.on('end', function() {
        if ((typeof field.minSize === 'number') && (uploadedSize < field.minSize))
            return response(false, 'File should be more than ' + field.minSize + 'MB');

        console.log('save temp:', file.path, this.name);
        response(true, 'File ' + info.name + ' saved in temp');
    });
};

proto.sendStatus = function(text) {
    if (!this.form.socket)
        return log.warn('Cant send status for file field, because not socket entity in form!');

    this.form.socket.emit('formFileStatus', {
        form: this.form.id,
        field: this.name,
        text: String(text)
    });
}

proto.getById = function(id, values) {
    if (util.isArray(values)) {
        //console.log("ok is array");
        var i;
        for (i = 0; i < values.length; i++) {
            if (values[i]._id == id) {
                return values[i];
            }
            //console.log(values[i]);
        }
    }

    return false;
}

module.exports = File;