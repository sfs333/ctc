var path = require('path');
var async = require('async');
var util = require('util');
var fs = require('fs.extra');
var lib = require('./../../lib');
var gm = require('gm').subClass({imageMagick: true});

module.exports = function(field, form) {
    form.on('submit', function (cb) {
        var data = field.get();
        if (!data) return cb();

        async.series([
            function(cb) {
                if (field.multiple || !field.value[0])
                    return cb();

                field.removePresets.apply(field, [field.value[0], cb]);
            },
            function(cb) {
                if (util.isArray(data.add))
                    return field.applyPresets(data.add, cb);
                else if (typeof data === 'object')
                    return field.applyPresets([data], cb);

                cb()
            }
        ], cb);
    });
    //Remove presets for one
    form.on('submit', function (cb) {
        if (field.multiple || !field.input || (field.input.length > 0) || !field.value[0])
            return cb();
        field.removePresets(field.value[0], cb);
    });

    //Remove presets for multiple
    form.on('submit', function (cb) {
        if (!field.input || !field.input.delete || (field.input.delete.length === 0))
            return cb();
        var workers = [];
        var worker = function(file) {
            workers.push(function(cb) {
                var info = (typeof field.value.id === 'function') ? field.value.id(file.id) : field.getById(file.id, field.value);
                //if (typeof field.value.id === 'function') {
                // var info = handler(file.id);

                if (!info)
                    return cb(new Error('File by id not find'));
                return field.removePresets(info, cb);
                // }
                //return cb(new Error('Not support remove by field name now'));



                //if (typeof field.value.id === 'function') {
                //    var info = field.value.id(file.id);
                //    if (!info)
                //        return cb('File by id not find');
                //
                //    return field.removePresets(info, cb);
                //}
                //return cb(new Error('Not support remove by field name now'));
            });
        }
        field.input.delete.forEach(worker);
        async.series(workers, cb);
    });
}