var modules = require('modules');
var async = require('async');
var File = require('./../file');
var util = require('util');
var path = require('path');
var fs = require('fs.extra');
var log = modules.use('log')(module);
var lib = require('./../../lib');
var gm = require('gm').subClass({imageMagick: true});
var bytes = require('bytes');

var Image = function (info, form) {
    var field = this;
    //this.template = 'file';
    this.construct(info, form);
    this.addClass('field-file');
    this.presets = info.presets || {};
    this.allowTypes = ['png', 'jpeg'];
    this.previewsViewName = 'imagePreviews';
    this.rewriteOriginal = info.rewriteOriginal || false;
    this.presetHandlers = {
        smartSize: require('./presets/smartSize'),
        watermark: require('./presets/watermark'),
        //cropByFace: require('./presets/cropByFace'),
        crop: require('./presets/crop'),
        maxSize: require('./presets/maxSize'),
        resize: require('./presets/resize')
    }
    //require('./../file/events')(field, form);
    require('./events')(field, form);
}

util.inherits(Image, File);
var proto = Image.prototype;
//proto.preset = function(file, name) {
//    console.log('ok');
//    console.log(file);
//    console.log(name);
//
//    return 'img file path';
//}
proto.viewData = function(items) {
    items.forEach(function(item) {
        item.publicPath = {
            value: ((item.presets && item.presets.fieldPreview) ? item.presets.fieldPreview.public : item.publicPath),
            params : { title: item.name, full: item.publicPath}
        };
    });
    return items;
}
proto.rewriteOriginalImage = function(file, cb) {
    var self = this,
        originalPath = file.path,
        rewrite = file.presets[self.rewriteOriginal];
    if (!rewrite) return cb();

    async.waterfall([
        function(cb) {
            gm(rewrite.path).identify(cb);
        },
        function(data, cb) {
            file.path = rewrite.path;
            file.publicPath = rewrite.public;
            file.size = data.Filesize ? bytes(data.Filesize.toLowerCase()) : 0;
            file.type = data["Mime type"] || 'image/jpeg';
            cb();
        },
        function(cb) {
            self.removeFile({ path: originalPath }, cb);
        }
    ], cb);
}
proto.applyPresets = function (files, cb) {
    var self = this,
        name,
        preset,
        preWorker,
        groupWorker,
        makeWorker,
        fileWorker,
        c,
        readyProcessCount = 0,
        countProcess = 0,
        percentLength = 0,
        results = {},
        workers = {pre:[], preGroup: [], group:{}, make: [], file: []};

    preWorker = function (preset) {
        workers.pre.push(function (cb) {
            preset.pre.apply(preset, [cb]);
        });
        return preset;
    }
    groupWorker = function(group, preset) {
        if (!workers.group[group])
            workers.group[group] = [];
        workers.group[group].push(function(info, cb) {
            async.waterfall([
                function(cb) {
                    //if (info.isFirst)
                    //    return cb();
                    async.waterfall([
                        function(cb) {
                            info.img.write(info.path, function(a, b, c) {
                                info.img = gm(info.path);
                                cb();
                            });
                        },
                        function(cb) {
                            info.img.size(cb);
                        },
                        function(size, cb) {
                            info.size = size;
                            cb();
                        }
                    ], cb);
                },
                function(cb) {
                    preset.make(info, cb);
                },
                function(cb) {
                    readyProcessCount++;
                    //console.log((percentLength * readyProcessCount) + '%');
                    self.sendStatus(util.format(
                        'file: <b>%s</b>, make <b>%s</b> preset, main progress - <b>%s%</b>',
                        info.name,
                        group,
                        (percentLength * readyProcessCount).toFixed(2)
                    ));
                    info.isFirst = false;
                    results[group] = info.path;
                    cb();
                }
            ], cb);
        });
        return preset;
    }
    makeWorker = function(handlers, group, dir) {
        workers.make.push(function(file, cb) {
            var img = gm(file.path);
            var filePath = path.join(dir, file.name);
            var info = {img: img, path: filePath, isFirst: true, name: file.name, dir: dir};
            info.img.autoOrient();
            info.img.noProfile();
            async.waterfall([
                function(cb) {
                    img.size(cb);
                },
                function (size, cb) {
                    info.size = size;
                    async.applyEachSeries(handlers, info, cb);
                },
                function(cb) {
                    info.img.write(filePath, cb);
                },
                function(a, b, c , cb) {
                    file.presets[group] = {
                        path: filePath,
                        public: path.join(self.inputDir, group, file.name)
                    };
                    cb();
                }
            ], cb);
        });
    };
    fileWorker = function(file) {
        workers.file.push(function(cb) {
            async.series([
                function(cb) {
                    async.applyEach(workers.make, file, cb);
                },
                function(cb) {
                    if (!self.rewriteOriginal)
                        return cb();
                    self.rewriteOriginalImage(file, cb);
                }
            ], cb);
        });
    };
    var preGroupWorker = function(group) {
        var dir = path.join(self.realInputDir, group);
        workers.preGroup.push(function(cb) {
            fs.exists(dir, function(exist) {
                if (exist) return cb();
                fs.mkdir(dir, cb);
            });
        });
        return dir;
    };

    this.eachPreset(function(group, info) {
        for (name in info) {
            if (!(preset = self.getPreset(name))) {
                log.warn('Not found preset ' + name);
                continue;
            }
            countProcess++;
            groupWorker(group, preWorker(new preset(info[name])));
        }
        makeWorker(workers.group[group], group, preGroupWorker(group));
    });
    percentLength = 100 / (countProcess * files.length);

    async.waterfall([
        function(cb) {
            async.parallel(workers.pre, cb);
        },
        function(res, cb) {
            async.parallel(workers.preGroup, cb);
        },
        function(res, cb) {
            files.forEach(function (file) {
                file.presets = {};
                fileWorker(file);
            });
            //async.parallel(workers.file, cb);
            async.series(workers.file, cb);
        },
    ], cb);
}
proto.getPreset = function (name) {
    return (this.presetHandlers[name]) ? this.presetHandlers[name] : false;
}
proto.eachPreset = function(cb) {
    var group;
    for (group in this.presets) {
        cb.apply(this, [group, this.presets[group]]);
    }
}
proto.presetFilePath = function(presetName, file) {
    return path.join(this.realInputDir, presetName, file.name);
}
proto.removePresets = function(file, cb) {
    var self = this,
        workers = [],
        worker;
    worker = function(path) {
        workers.push(function(cb) {
            self.removeFile({path: path}, cb);
        });
    }
    this.eachPreset(function(name, info) {
        worker(self.presetFilePath(name, file));
    });
    async.series(workers, cb);
}

module.exports = Image;