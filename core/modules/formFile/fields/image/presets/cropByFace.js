var lib = require('./../../../lib');
var path = require('path');
var fs = require('fs.extra');
var async = require('async');
var util = require('util');
var cv  = require('opencv');

var cbf = module.exports = function(info) {
    this.width = info.width || 640;
    this.height = info.height || 480;
    this.heightAdd = info.heightAdd || 0;
    this.mode = info.mode || 'smartSize';
    this.debug = false;
}

var proto = cbf.prototype;
proto.pre = function(cb) {
    cb();
}
proto.make = function(info, cb) {
    var self = this;
    self.handler = require('./' + this.mode);

    async.waterfall([
        function(cb) {
            self.getMiddlePoint(info.img.source, (info.dir + '/res.jpeg'), cb);
        },
        function(middle, cb) {
            middle.y = middle.y + ((info.size.height / 100) * self.heightAdd);
            var handler = new self.handler({width: self.width, height: self.height, middlePoint: middle});
            handler.make(info, cb);
        },
        function(cb) {
            cb();
        }
    ], cb);
}

proto.getMiddlePoint = function(path, save, cb) {
    var cascade = '/home/pm/projects/ctc-2/node_modules/opencv/data/haarcascade_frontalface_alt2.xml',
    //var cascade = '/home/pm/projects/ctc-2/node_modules/opencv/data/haarcascade_frontalface_alt.xml',
        self = this,
        minSize = 100,
        maxSize = 250,
        xSum = 0,
        ySum = 0,
        count = 0,
        middle = {x:false, y:false};

    cv.readImage(path, function(err, im) {
        try {
            im.detectObject(cascade, {}, function(err, faces) {
                count = faces.length;
                for (var i=0;i<faces.length; i++) {
                    var face = faces[i];
                    if ((face.width < minSize) || (face.height < minSize) || (face.width > maxSize) || (face.height > maxSize)) {
                        count--;
                        continue;
                    }
                    ySum += face.y + (face.height / 2);
                    xSum += face.x + (face.width / 2);
                    //debug
                    if (self.debug) {
                        console.log(face.width + 'x' + face.height);
                        im.ellipse(
                            face.x + face.width/2,
                            face.y + face.height/2,
                            face.width/2,
                            face.height/2
                        );
                    }
                }
                if (count > 0) {
                    middle.x = xSum / count;
                    middle.y  = ySum / count;
                }
                if (self.debug) {
                    im.line([middle.x,middle.y - 30], [middle.x, middle.y + 30]);
                    im.line([middle.x - 30,middle.y], [middle.x + 30, middle.y]);
                    im.save(save);
                }

                cb(null, middle);
            });
        } catch(err) {
            cb(err);
        }

    });
}
