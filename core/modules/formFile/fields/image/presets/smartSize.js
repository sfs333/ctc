var gm = require('gm').subClass({imageMagick: true});

var ss = module.exports = function(info) {
    this.width = info.width || 640;
    this.height = info.height || 480;
    this.mp = info.middlePoint || {x:false, y:false};
}

var proto = ss.prototype;
proto.pre = function(cb) {
    cb();
}
proto.resizeByWidth = function(info) {
    var newWidth = this.height * (info.size.width / info.size.height);
    if (newWidth < this.width)
        return this.resizeByHeight(info);
    this.mp.x = this.mp.x ? (this.mp.x / (info.size.width / newWidth)) : (newWidth / 2);
    this.mp.y = this.height / 2;
    var x = this.mp.x - (this.width / 2);
    info.img.resize(null, this.height)
        .crop(this.width, this.height, ((x >= 0 ) ? x : 0).toFixed(0), 0);
}

proto.resizeByHeight = function(info) {
    var newHeight = this.width * (info.size.height / info.size.width);
    if (newHeight < this.height)
        return this.resizeByWidth(info);
    this.mp.x = this.width / 2;
    this.mp.y = this.mp.y ? (this.mp.y / (info.size.height / newHeight)) : (newHeight / 2);
    var y = this.mp.y - (this.height / 2);
    info.img.resize(this.width)
        .crop(this.width, this.height, 0, ((y >= 0 ) ? y : 0).toFixed(0));
}

proto.make = function(info, cb) {
    if (info.size.width > info.size.height)
        this.resizeByWidth(info);
     else
        this.resizeByHeight(info);

    cb();
}
