var path = require('path');
var fs = require('fs.extra');
var async = require('async');
var moduleManager = require('moduleManager');
var log = moduleManager.use('log')(module);

var systemPath = path.normalize(__dirname + '/../../../public');
var tempPath = path.join(systemPath, 'files/temp');

var initTempDir = function() {
    async.waterfall([
        function(callback) {
            fs.exists(tempPath, function(exist) {
                callback(null, exist);
            });
        },
        function(exist, callback) {
            if (!exist)
                return callback();

            fs.rmrf(tempPath, callback);
        },
        function(callback) {
            fs.mkdir(tempPath, callback);
        }
    ], function(err) {
        if (err) return log.error(err);
    });
}

initTempDir();
module.exports.systemPath = systemPath;
module.exports.tempPath = tempPath;
module.exports.schema = require('./models/file');
module.exports.imgSchema = require('./models/image');