var modules = require('modules');
var publicDepends = modules.use('publicDepends');

module.exports.run = function(app, cb) {
    cb();
}

publicDepends.addDependency({
    checkboxSwitch: {
        scripts: ['js/toggle/bootstrapToggle', 'js/toggle/index'],
        dependencies: ['app']
    },
    form: {
        dependencies: ['checkboxSwitch']
    }
});