module.exports.run = function(app, callback) {
    // catch 404 and forward to error handler
    app.use(function(req, res, next) {
        var err = new Error('Page Not Found');
        err.status = 404;
        next(err);
    });

    // error handlers
    // development error handler
    if (app.get('env') === 'development') {
        app.use(function(err, req, res, next) {
            res.status(err.status || 500);
            res.render((err.template || 'error'), {
                message: err.message,
                error: err,
                title: 'Error (' + err.status + ')'
            });
        });
    }

    // production error handler
    // no stacktraces leaked to user
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render((err.template || 'error'), {
            message: err.message,
            error: {},
            title: 'Error'
        });
    });

    callback(null);
};