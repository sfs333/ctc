var modules = require('modules');
var async = require('async');
var path = require('path');
var fs = require('fs');
var ejs = require('ejs');
var app = require('app');
var config = modules.use('config');
var redisStorage = modules.use('redisStorage');
var log = modules.use('log')(module);
var logging = config.get("ejsTools:logging");
var locals = modules.use('locals');
var _ = require("underscore");

var cacheItemsValues = {};

var getCacheName = function(filePath) {
    return String('template_' + filePath);
};

var getFileData = function(params, callback) {
    var theme = params.theme || false;
    var filePath = path.join(app.get('basePath'), 'themes', (theme || config.get('theme')), 'templates', (params.path + '.ejs'));

    if (logging)
        log.info(filePath);
    var cacheKey = getCacheName(filePath);
    async.waterfall([
        function(callback) {
            if (config.get("ejsTools:cacheTemplates") && cacheItemsValues[cacheKey])
                redisStorage.get(cacheKey, callback);
            else
                callback(null, false);
        },
        function(cacheString, callback) {
            if (cacheString && (cacheString.length > 0))
                return callback(null, cacheString);

            async.waterfall([
                function(callback) {
                    fs.exists(filePath, function(exist) {
                        if (!exist)
                            return callback(new Error('File ' + filePath + ' not exist, you should create template'));

                        fs.readFile(filePath, callback);
                    });
                },
                function(fileData, callback) {
                    var valueString = fileData.toString();
                    if (valueString.length <= 0)
                        return callback(new Error('Template ' + filePath + ' is empty'));

                    redisStorage.set(cacheKey, valueString, function(err) {
                        callback(err, valueString);
                        cacheItemsValues[cacheKey] = true;
                    });
                }

            ], callback);
        }
    ], callback);
};

var render = function (valueString, variables) {
    return ejs.render(valueString, _.extend({}, locals, (variables || {})));
};

var read = function(params, callback) {
    async.waterfall([
        function(callback) {
            getFileData(params,callback);
        },
        function(valueString, callback) {
            callback(null, render(valueString, params.variables));
        }
    ], callback);
};

module.exports.read = read;
module.exports.render = render;
module.exports.getFileData = getFileData;