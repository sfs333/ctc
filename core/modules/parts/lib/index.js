var moduleManager = require('moduleManager');
var util = require('util');
var async = require('async');
var log = moduleManager.use('log')(module);
var ejsTools = moduleManager.use('ejsTools');

var defCb = function(err) {
    if (err) return log.error(err);
}

var Parts = function() {
    this.handlers = {};
}
var proto = Parts.prototype;

proto.renderByTemplate = function(info, context, cb) {
    async.waterfall([
        function(cb) {
            if (typeof info.getVariables === 'function')
                info.getVariables(context, cb);
            else
                cb(null, {});
        },
        function(variables, cb) {
            ejsTools.read({
                path: info.template,
                absolutePath: false,
                theme: info.theme,
                variables: variables
            }, cb);

        }], cb);
}
proto.renderByHandler = function(info, context, cb) {
    info.getHtml(context, cb);
}
proto.render = function(id, context, cb) {
    var self = this;
    if (typeof cb !== 'function')
        cb = defCb;
    if (typeof id !== 'string')
        return cb(new Error(util.format('Don`t correct id part %s', id)));

    var info = {};
    if (typeof info.theme !== 'string')
        info.theme = false;
    if (!(info = this.get(id)))
        return cb(new Error(util.format('Part %s not register!', id)));

    var response = {id: id, html: '', params: {}};
    async.waterfall([
        function(cb) {
            if (info.template)
                return self.renderByTemplate(info, context, cb);
            else if (typeof info.getHtml === 'function')
                return self.renderByHandler(info, context, cb);
            else
                cb(null, '');
        },
        function(html, cb) {
            response.html = html;
            response.params = info.params || {};
            cb(null, response);
        }
    ], cb);
}

proto.add = function(id, info) {
    if (typeof info !== 'object')
        return false;

    if (this.get(id))
        log.warn('Rewriting part handler - ' + id);

    this.set(id, info);
}

proto.get = function(id) {
    return this.handlers[id] || null;
}

proto.set = function(id, info) {
    this.handlers[id] = info;
}

var parts = new Parts();
module.exports = parts;