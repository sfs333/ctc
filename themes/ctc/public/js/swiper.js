(function(app) {

    var galleryTemplate =
        '<div class="swiper-container gallery-top"><div class="swiper-wrapper"></div>' +
            '<div class="swiper-button-next swiper-button-white"></div>' +
            '<div class="swiper-button-prev swiper-button-white"></div>' +
         '</div>';

    var thumbsTemplate = '<div class="swiper-container gallery-thumbs"><div class="swiper-wrapper"></div></div>';

    var galleryThumbsPadding = 10;
    var popup = app.modules.popup;
    var async = app.modules.async;
    var swiper = app.modules.swiper = {};

    swiper.slider = function($obj) {
        new Swiper($obj, {
            pagination: $obj.find('.swiper-pagination'),
            paginationClickable: $obj.find('.swiper-pagination'),
            nextButton: $obj.find('.swiper-button-next'),
            prevButton: $obj.find('.swiper-button-prev'),
            spaceBetween: 30,
            effect: app.isMobile.any() ? 'slide' : 'fade',
            autoplay: $obj.data('autoplay') || false,
            speed: 500,
            preloadImages: false,
            updateOnImagesReady: false,
            lazyLoading: true
        });
    }

    swiper.galleryThumbs = function($obj) {
        $obj.find('.swiper-slide').on('click', function(e) {
            e.preventDefault();
            var $self = $(this);
            var html;
            var $item;
            var $items = $self.siblings('a.swiper-slide');

            async.waterfall([
                function(cb) {
                    var $content = $(galleryTemplate + thumbsTemplate),
                        $top = $content.siblings('.gallery-top'),
                        $thumbs = $content.siblings('.gallery-thumbs'),
                        item;

                    $items.each(function() {
                        $item = $(this);
                        item = '<div class="swiper-slide" style="background-image:url(' + $item.attr('href') + ')"></div>';
                        $top.find('.swiper-wrapper').append($(item));
                        $thumbs.find('.swiper-wrapper').append($(item));
                    });
                    cb(null, $content);
                },
                function($content, cb) {
                    popup.show($content, {class: 'swipe-gallery-popup'}, cb);
                },
                function($obj, cb) {
                    var $modal = popup.wrapper();

                    //$modal.find('.modal-dialog').width(1000);
                    var $top = $modal.find('.gallery-top');
                    var $thumbs = $modal.find('.gallery-thumbs');
                    var galleryTop = new Swiper($top, {
                        nextButton: $top.find('.swiper-button-next'),
                        prevButton: $top.find('.swiper-button-prev'),
                        spaceBetween: 10,
                        effect: 'fade'
                    });

                    async.parallel([
                        function(cb) {
                            //$top.animate({height: 450 + 'px'}, 500, cb);
                            $modal.find('.gallery-thumbs').css({
                                paddingTop: galleryThumbsPadding + 'px',
                                paddingBottom: galleryThumbsPadding + 'px'
                            })
                            $top.height((($(window).height() - $thumbs.height()) - (2 * galleryThumbsPadding)), cb);
                        },
                        function(cb) {
                            //$thumbs.animate({height: 100 + 'px'}, 500, cb);
                            //$thumbs.height(100, cb);
                            cb();
                        },
                        function(cb) {
                            var galleryThumbs = new Swiper($thumbs, {
                                spaceBetween: 10,
                                centeredSlides: true,
                                slidesPerView: 'auto',
                                touchRatio: 0.2,
                                slideToClickedSlide: true
                            });
                            galleryTop.params.control = galleryThumbs;
                            galleryThumbs.params.control = galleryTop;
                            cb();
                        }
                    ], cb);
                }
            ]);

            return false;
        });
    }

    swiper.gallery = function($obj) {
        $obj.find('.swiper-slide').on('click', function(e) {
            e.preventDefault();
            var $self = $(this);
            var html;
            var $item;
            var initial = 0;
            //var $items = $self.siblings('a.swiper-slide');
            var $items = $self.parent('.swiper-wrapper').find('a.swiper-slide');
            async.waterfall([
                function(cb) {
                    var $content = $(galleryTemplate),
                        $wrapper = $content.find('.swiper-wrapper'),
                        item;

                    $items.each(function(key) {
                        $item = $(this);
                        item = '<div class="swiper-slide" style="background-image:url(' + $item.attr('href') + ')"></div>';
                        $wrapper.append(item);
                        if ($item.hasClass('swiper-slide-active'))
                            initial = key;
                    });
                    cb(null, $content);
                },
                function($content, cb) {
                    popup.show($content, {class: 'swipe-gallery-popup'}, cb);
                },
                function($content, cb) {
                    var $modal = popup.wrapper();
                    var $top = $modal.find('.gallery-top');
                    $top.height($(window).height(), cb);
                    new Swiper($top, {
                        nextButton: $top.find('.swiper-button-next'),
                        prevButton: $top.find('.swiper-button-prev'),
                        spaceBetween: 10,
                        effect: 'fade',
                        initialSlide: initial,

                        preloadImages: false,
                        updateOnImagesReady: false,
                        lazyLoading: true
                    });
                }
            ]);

            return false;
        });
    }

    app.on('load', function() {
        $('.swiper-container').each(function() {
            swiper.slider($(this));
        });
        $('.swiper-gallery-thumbs').each(function() {
            swiper.galleryThumbs($(this));
        });
        $('.swiper-gallery').each(function() {
            swiper.gallery($(this));
        });
    });
})(app);