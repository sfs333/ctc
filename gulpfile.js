'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require("gulp-rename");

gulp.task('sass', function () {
    return gulp.src('./themes/ctc/sources/scss/main.scss ')
        .pipe(sass().on('error', sass.logError))
        .pipe(rename(function (path) {
            path.basename = "ctc-main";
        }))
        .pipe(gulp.dest('./themes/ctc/public/css'));
});

gulp.task('sass:admin', function () {
    return gulp.src('./themes/admin/sources/scss/main.scss ')
        .pipe(sass().on('error', sass.logError))
        .pipe(rename(function (path) {
            path.basename = "admin-main";
        }))
        .pipe(gulp.dest('./themes/admin/public/css'));
});

gulp.task('sass:watch', function () {
    gulp.watch('./themes/tuz/sources/scss/**/*.scss', ['sass']);
});