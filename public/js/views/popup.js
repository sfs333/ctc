(function(app) {
    var view = app.modules.view;
    app.on('popupInit', view.attachEvents);
})(app);