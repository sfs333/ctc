(function(app) {
    var addRowToTable = function($view, info) {
        console.log(info);
        var $tbody = $view.find('tbody');
        var $lastChild = $tbody.find('tr:last-child');
        var $tr = null;
        var html = '';
        $view.find('thead').find('th').each(function() {
            var name = $(this).find('a').first().attr('data-name');
            if (name && info.items[name])
                html += ('<td>' + info.items[name] + '</td>');
        });

        if (info.isNew) {
            $tr = $('<tr></tr>');
            $tr.attr('id', info.id);
            $tr.attr('item-id', info.id);
            $tr.addClass($lastChild.hasClass('even') ? 'odd' : 'even');
            $tbody.append($tr);
        } else {
            $tr = $tbody.find('#' + info.id);
        }
        $tr.html(html);
        return $tr;
    };

    var addRowToList = function($view, info) {

    };

    app.actions.addViewRow = function (info) {
        if (!info.view)
            return console.error('For addTableRow action you should send "view" this is view name');

        var $view = $('.view-' + info.view);

        if ($view.length === 0)
            return console.error('Not find view ' + info.view);

        if (typeof info.items !== 'object')
            info.items = {};

        if (!info.id)
            info.id = '';

        var handler = ($view.find('.view-table').length > 0) ? addRowToTable : addRowToList;
        var rendered = handler($view, info);

        if (rendered)
            app.emit('htmlPart', rendered);
    };
})(app);