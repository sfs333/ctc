(function(app) {
    var popup;
    app.modules.popup = popup = {};
    var parts = app.modules.parts;
    popup.htmlLoad;
    popup.message;
    popup.confirm;
    popup.open;
    popup.wrapper;
    popup.formEventsPopup;
    popup.attachEvents;
    var $popupWrapper;


    var getWrapper = function() {
        if (!$popupWrapper)
            createWrapper();

        return $popupWrapper;
    };

    app.actions.closePopup = function (info, form) {
        var wrapper = getWrapper();
        if (wrapper)
            getWrapper().dialog('close');
    };

    var createWrapper = function() {
        $popupWrapper = $('<div class="popup-wrapper"></div>').hide();
        $('body').prepend($popupWrapper);
    };

    var defaultDialog = {
        modal: true,
        draggable: false,
        resizable: false,
        width: '628px',
        dialogClass: false,
        close: function() {
            getWrapper().html('');
        }
    };

    popup.confirm = function(message, okCb, noCb, params) {
        params = params || {};
        if (!params.buttons) {
            params.buttons = {
                accept: function() {
                    if (typeof okCb == 'function')
                        okCb.apply(this, []);
                    $( this ).dialog( "close" );
                },
                cancel: function() {
                    if (typeof noCb == 'function')
                        noCb.apply(this, []);
                    $( this ).dialog( "close" );
                }
            }
        }
        if (!params.title)
            params.title = 'Confirmation';
        params.dialogClass = 'confirm-popup' + (params.dialogClass ? (' ' + params.dialogClass) : '');
        var popupWrapper = getWrapper();
        popupWrapper
            .html('')
            .append('<p class="popup-confirm">' + message + '</p>');

        popupWrapper.dialog($.extend(defaultDialog, params));
    }

    popup.attachEvents = function(context) {
        $('.popup-btn', context).on('click', function(event) {
            popup.open($(this).data());
            event.preventDefault();
            return false;
        });
    }

    app.on('load', function() {
        var popupWrapper = getWrapper();
        var htmlLoad = function(params, wrapper, callback) {
            if (typeof params != 'object')
                params = {};

            if (!params.name)
                return callback(app.error('Don`t correct name for popup loader'));
            parts.render(params.name, params, function(err, data) {
                if (err) return callback(err);

                if (typeof data.html != 'string')
                    return callback(app.error('Don`t correct response for popup loader'));

                var content = $(data.html);
                wrapper.html(content);

                app.emit('popupInit', content);
                app.emit(( data.name + 'Init'), content);
                content.find('form.app-form').each(function() {
                    var form = $(this);
                    app.emit('formAddEvents', form);
                });
                callback(err, {params: data.params, content: content});
            });
        }

        var formEventsPopup = function(content) {
            var popupWrapper = getWrapper();
            content.find('form.app-form').each(function() {
                $(this).find('.form-cancel-button').on('click', function(e) {
                    popupWrapper.dialog('close');
                });
            });

            $('.ui-widget-overlay').on('click', function(e) {
                popupWrapper.dialog('close');
            });
        }

        var openPopup = function(params) {
            var popupWrapper = getWrapper();
            htmlLoad(params, popupWrapper, function(err, data) {
                if (err) return console.error(err.message);

                if (!data.params.title && params.title)
                    data.params.title = params.title;
                popupWrapper.dialog($.extend(defaultDialog, data.params));
                formEventsPopup(data.content);
            });
        }

        var showMessage = function(message, params) {
            params = params || {};
            if (!params.buttons) {
                params.buttons = {
                    ok: function() {
                        $( this ).dialog( "close" );
                    }
                }
            }
            if (!params.title)
                params.title = 'Message';

            var popupWrapper = getWrapper();
            popupWrapper
                .html('')
                .append('<p class="popup-message">' + message + '</p>');

            popupWrapper.dialog($.extend(defaultDialog, params));
        }

        popup.attachEvents($('body'));

        if (app.values.message && (typeof app.values.message == 'string')) {
            showMessage(app.values.message);
        }

        popup.htmlLoad = htmlLoad;
        popup.message = showMessage;
        popup.open = openPopup;
        popup.wrapper = getWrapper();
        popup.formEventsPopup = formEventsPopup;
    });
})(app);