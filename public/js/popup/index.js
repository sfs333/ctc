(function(app) {
    var popup = app.module('popup', {});
    var parts = app.module('parts');
    var async = app.module('async');
    popup.confirm;
    popup.open;
    popup.message;
    popup.wrapper;
    popup.content;
    var $popupWrapper = false;
    popup.template = '<div class="modal fade">'+
    '<div class="modal-dialog">'+
    '<div class="modal-content">'+
    '<div class="modal-header">'+
    '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>'+
    '<h4 class="modal-title">Modal</h4></div>'+
    '<div class="modal-body"></div>'+
    '<div class="modal-footer">' +
    '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>';

    var wrapper = popup.wrapper = function() {
        if ($popupWrapper) return $popupWrapper;

        var $wrap = popup.createWrapper();
        $('body').prepend($wrap);
        return $wrap;
    };

    app.actions.closePopup = function (info, form) {
        $('.modal').modal('hide');
    };
    app.actions.popupMessage = function (info, form) {
        popup.message(info.text, info);
    };

    app.on('popupEvents', function(context) {
        $('.popup-btn, .popup-marker', context).on('click', function(event) {
            event.preventDefault();
            popup.open($(this).data());
            return false;
        });
    });

    app.on('htmlPart', function($el) {
        app.emit('popupEvents', $el);
    });

    popup.createWrapper = function(className) {
        $popupWrapper = $(popup.template)
            .hide()
            .modal({ show: false })
            .on('hidden.bs.modal', function() {
                var $slef = $(this);
                $slef.find('.modal-title').text('Modal');
                $slef.find('.modal-body').html('');
                $slef.attr('class', 'modal fade');
                $slef.find('.modal-footer').hide();
            })
            .on('hide.bs.modal', function() {
                $(this).addClass('hiding');
            });
        $popupWrapper.find('.modal-footer').hide();
        if (className)
            $popupWrapper.addClass(className);

        return $popupWrapper;
    }

    app.on('load', function() {
        var popupWrapper = wrapper();

        var show = popup.show = function(content, params, cb) {
            params = params || {};
            var $wrapper = params.wrapper || wrapper();
            var showPopup = function() {
                if (content)
                    $wrapper.find('.modal-body').html('').append(content);
                $wrapper.find('.modal-title').text(params.title || '');
                if (params.class)
                    $wrapper.addClass(params.class);
                if (params.footer)
                    $wrapper.find('.modal-footer').show();

                if (typeof cb === 'function')
                    $wrapper.one('shown.bs.modal', function($obj) {
                        cb.apply($wrapper, [null, $obj]);
                    });
                $wrapper.modal('show');
            }
            if ($wrapper.is(":visible")) {
                if (!$wrapper.hasClass('hidding'))
                    $wrapper.modal('hide');

                $wrapper.one('hidden.bs.modal', function() {
                    showPopup();
                });
            } else {
                showPopup();
            }
        }
        var open = popup.open = function(params, cb) {
            var $popupWrapper = wrapper();
            if (typeof cb !== 'function')
                cb = function(err) { if (err) return console.error(err) }
            if (typeof params != 'object')
                params = {};

            async.waterfall([
                function(cb) {
                    if (!params.name)
                        return cb(app.error('Don`t correct name for popup loader'));

                    parts.render(('popup_' + params.name), params, cb);
                },
                function(data, cb) {
                    $popupWrapper.find('.modal-body').html(data.$el);
                    app.emit('popupInit', data.$el);
                    app.emit(( data.name + 'Init'), data.$el);

                    params.title = (!data.params.title && params.title) ? params.title : data.params.title;
                    if (data.params.dialogClass)
                        params.class = data.params.dialogClass;

                    params.wrapper = $popupWrapper;
                    show(false,  params);
                    cb();
                }
            ], cb);
        };

        var message = popup.message = function(message, params) {
            show('<p class="popup-message">' + message + '</p>', params);
        };

        if (app.values.message && (typeof app.values.message == 'string'))
            message(app.values.message);
    });
})(app);