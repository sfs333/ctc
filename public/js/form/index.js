(function(app) {
    var parts = app.module('parts');
    var formHandlers = {
        input: function (field) {
            switch (field.attr('type')) {
                case 'checkbox':
                    return Boolean(field.is(':checked'));
                    break;
                case 'radio':
                    return field.parents('.radios-wrapper').find('input[name="' + field.attr('name') + '"]:checked').val();
                    break;
                default:
                    return field.val();
                    break;
            }
        },
        select: function (field) {
            return field.find(":selected").val();
        },
        textarea: function (field) {
            if (field[0].ckeditor)
                field[0].ckeditor.updateElement();
            return field.val();
        }
    };

    var getErrorWrapper = function(form) {
        var custom = form.data('.error-wrapper-selector');
        return (custom ? $(custom) : form.find('div.alert-danger'));
    };

    app.actions.formErrorValidate = function (info, form) {
        var message = info.message || 'error validate';

        if (typeof info.field == 'string') {
            form.find('[name="' + info.field + '"]')
                .parents('.form-item')
                .addClass('error')
                .append('<ul class="messages"><li>' + message + '</li></ul>');
        } else {
            getErrorWrapper(form).text(message).show();
        }

        app.emit(app.tools.getListenerName(info.type, 'errorValidate'), form, info);
    };

    app.actions.formSuccessValidate = function (info, form) {
        app.emit(app.tools.getListenerName(info.type, 'successValidate'), form, info);
    };

    app.actions.resetForm = function (form) {
        getErrorWrapper(form).text('').hide();
        form.find('.form-item').removeClass('error');
        form.find('ul.messages').remove();
    };

    app.actions.lockForm = function (form) {
        form.find('.form-actions input, .form-actions a')
            .addClass('disabled')
            .attr('disabled', 'disabled');
        form.find('.preloader').fadeIn();
    };

    app.actions.unlockForm = function (form) {
        form.find('.preloader').fadeOut();
        form.find('.form-actions input, .form-actions a')
            .removeClass('disabled')
            .attr('disabled', false);
    };

    app.on('load', function () {
        var getFieldValue = function(field) {
            var handler = field.prop("tagName").toLowerCase();
            if (typeof formHandlers[handler] == 'function')
                return formHandlers[handler](field);

            console.error(app.error('System don`t have ' + handler + ' handler.'));
            return false;
        };

        var validateField = function(field, form) {
            if (!field || !(field instanceof $))
                return console.error('Error validate field, field should be jQuery object ' + form.attr('id'));

            var value = getFieldValue(field);

            if (!value && (typeof value == 'boolean'))
                return console.warn('Error getting field value in ' + form.attr('id'));

            app.action('resetForm', form);
            var data = {
                id: form.attr('id'),
                field: String(field.attr('name')),
                value: value
            };

            app.backend.emit('validateField', data, function (err, response) {
                if (err) return console.error(err);

                if (typeof response.status == 'number')
                    console.log("Server response with status " + response.status);

            }, form);
        };

        var submitForm = function(data, form) {
            app.emit('submitForm', form);
            form.find('input, select, textarea').each(function () {
                if (typeof this.tagName == 'string' && $(this).attr('type') != 'submit') {
                    var name = false;
                    if (name = $(this).attr('name')) {
                        var handler = this.tagName.toLowerCase();
                        if (typeof formHandlers[handler] == 'function') {
                            data.fields[name] = formHandlers[handler]($(this));
                        } else {
                            console.error('System don`t have ' + handler + ' handler.');
                        }
                    } else {
                        console.error('Teg don`t have name attribute');
                    }
                }
            });
            app.action('resetForm', form);
            app.action('lockForm', form);

            app.backend.emit('formProcess', data, function (err, data) {
                app.action('unlockForm', form);
                app.emit('submittedForm', form);
                if (err) return console.error('Error in form process');

                if (typeof data.status == 'number')
                    console.log("Server response with status " + data.status);

            }, form);
        };

        var events = function(form) {
            var id = form.attr('id');
            app.backend.emit('formRegister', {
                id: id
            });
            form.on('submit', function (event) {
                event.preventDefault();

                var data = {
                    id: id,
                    fields: {},
                    submit: true,
                    params: app.tools.getData(form)
                };

                submitForm(data, form);
            });

            form.find('input[active-validate="true"], select[active-validate="true"], textarea[active-validate="true"]').change(function () {
                validateField($(this), form);
            });

            form.find('.form-cancel-button').on('click', function(e) {
                form.fadeOut(150, function() {
                    form.remove();
                });
                e.preventDefault();
                return false;
            });

            form.find('textarea').each(function() {
                var self = $(this);
                //self[0].ckeditor = CKEDITOR.replace(self.attr('id'));
                //console.log(CKEDITOR.replace($(this).attr('id')).updateElement());
            });
        };

        app.on('formEvents', function($context) {
            if ($context.hasClass('app-form'))
                events($context);
            $('.app-form', $context).each(function() {
                events($(this));
            });

            $('.app-form-marker', $context).each(function() {
                var $self = $(this);
                var parent = $self.parent();
                var formName = $self.attr('id');
                var params = $self.data();
                params.formName = formName;
                parts.render('loadForm', params, function(err, data) {
                    if (err)
                        return console.error('error load form');

                    $self.remove();
                    var form = data.$el.hide();
                    parent.append(form);
                    //form.fadeIn(300);
                    form.show();
                    form.addClass('form');
                    app.emit('loadedForm', form);
                    app.emit(('load' + app.tools.getListenerName(app.tools.ucFirst(formName + 'Form'))), form);
                    //app.emit('formEvents', form);
                });
            });
        });

        app.on('htmlPart', function($el) {
            app.emit('formEvents', $el);
        });

        // app.emit('formEvents', $('body'));
    });

})(app);