(function (app) {
    app.on('popupLoginInit', function(loginPopup) {
        var wrapper = loginPopup.parents('.popup-wrapper');
        var popup = app.module('popup');
        loginPopup.find('.register-link').on('click', function(e) {
            popup.wrapper.dialog('close');
            popup.open({name: 'popupRegister'});
            e.preventDefault();
            return false;
        });
    });
})(app);