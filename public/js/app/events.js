(function(app) {
    app.listeners = {};

    app.on = function(name, listener) {
        if (typeof name != 'string')
            return console.error('Error when add event listener - event name should be string');

        if (typeof listener != 'function')
            return console.error('Error when add event listener - listener should be function');

        if (!app.listeners[name])
            app.listeners[name] = [];

        app.listeners[name].push(listener);
    };

    app.emit = function(name) {

        if (typeof app.listeners[name] != 'object')
            return console.log('Not exist listeners for ' + name);

        console.log('Event - ' + name + ' run, for ' + app.listeners[name].length + ' listeners.');
        var args = Array.prototype.slice.call(arguments).splice(1);
        app.listeners[name].forEach(function(listener, index) {
           listener.apply(app, args.concat(index));
        });
    };

    window.onload = function (){
        app.emit('load');
    };

})(app);