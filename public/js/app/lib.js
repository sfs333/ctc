(function(app) {
    app.handlerManager = function() {
        this.run = function(handler, value) {
            if (!handler || (typeof this[handler] != 'function'))
                return console.error('No correct handler ' + handler);

            this[handler](value);
        }
    }

    app.error = function(message) {
        return {message: String(message)};
    }

    app.module = function(/*..params..*/) {
        switch (arguments.length) {
            case 1:
                return app.modules[arguments[0]] || false;
            break;
            case 2:
                return app.modules[arguments[0]] = arguments[1];
            break;
            default:
                return false;
            break;
        }
    }

    app.isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (this.Android() || this.BlackBerry() || this.iOS() || this.Opera() || this.Windows());
        }
    };
})(app);

