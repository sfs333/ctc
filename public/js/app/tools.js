(function(app) {
    app.tools.generateID = function(n) {
        n = n || 30;
        var s = '';
        while(s.length < n)
            s += String.fromCharCode(Math.random() *127).replace(/\W|\d|_/g,'');

        return s;
    }

    app.tools.getData = function(object) {
        var attributes = object.get(0).attributes,
            name,
            response = {};

        for (var attrName in attributes) {
            if (!attributes[attrName] || (typeof attributes[attrName].nodeName != 'string'))
                continue;

            name = attributes[attrName].nodeName
            if (name.substring(0, 5) === "data-")
                response[$.camelCase(name.substring(5))] = object.attr(name);
        }

        return response;
    }

    app.tools.removeData = function(object) {
        var attributes = object.get(0).attributes, name;
        for (var attrName in attributes) {
            if ((name = attributes[attrName].nodeName) && (name.substring(0, 5) === "data-"))
                object.removeAttr(name);

        }
    }
    app.tools.getNameBrowser = function() {
        var ua = navigator.userAgent;
        if (ua.search(/Chrome/) > 0) return 'googleChrome';
        if (ua.search(/Firefox/) > 0) return 'firefox';
        if (ua.search(/Opera/) > 0) return 'opera';
        if (ua.search(/Safari/) > 0) return 'safari';
        if (ua.search(/MSIE/) > 0) return 'internetExplorer';
        return false;
    }

    app.tools.ucFirst = function (str) {
        return str.charAt(0).toUpperCase() + str.substr(1);
    }

    app.tools.isArray = function (obj) {
        return Boolean(obj.toString == Array.prototype.toString);
    }

    app.tools.getListenerName = function() {
        return Array.prototype
            .slice
            .call(arguments)
            .splice(0)
            .join('-')
            .replace(/\W+(.)/g, function (x, chr) {
                return chr.toUpperCase();
            });
    }

})(app);