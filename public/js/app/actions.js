(function(app) {
    app.actions.redirect = function(params) {
        if (typeof params.url == 'string') {
            var currentPath = window.location.href;
            var path = (params.url.length > 0) ? ((params.absolute ? '' : app.baseUrl) + params.url) : currentPath;
            path = path.replace('#', ''); //for IE
            window.location.replace(path);
        }
    };
    app.actions.timeoutAction = function(params) {
        setTimeout(function() {
            app.action(params.action, params.attributes);
        }, (params.time || 1000));
    };
    app.actions.messageWrap = function (info) {
        var type = info.type || 'info';
        var $div = $('<div class="system-message alert alert-' + type + '">' + (info.text || 'message' ) + '</div>').hide();

        var wrap = null;
        if (info.id)
            wrap = $('#' + info.id);
        else if (info.selector)
            wrap = $(info.selector);
        else
            wrap = $('.page');

        wrap.find('.system-message').remove();
        wrap.prepend($div);
        $div.fadeIn(300);
        setTimeout(function() {
            $div.fadeOut(300);
        }, 1500);
    };
    app.actions.actionById = function (info) {
        $('#' + info.id)[info.action](info.params || null);
    };
    app.action = function(action, attributes, context) {
        if (typeof app.actions[action] != 'function')
            return console.error(app.error('Action - ' + action + '  don`t exist'));

        app.actions[action]((attributes || null), context);
    };
})(app);