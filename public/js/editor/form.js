(function(app) {
    app.on('load', function () {
        var editor = app.module('editor');
        app.on('formEvents', function(form) {
            var field = form.find('.form-item.summernote-editor .form-control');
            if (field.length <= 0)
                return;

            editor.attach(field);
        });
        // app.on('submitForm', function(form) {
        //     var field = form.find('.form-item.summernote-editor .form-control');
        //     if (field.length <= 0)
        //         return;
        //
        //     field.html(field.code());
        // });
    });
})(app);