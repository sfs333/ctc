(function(app) {
    var parts = app.module('parts', {}),
     async = app.module('async'),
        switchClassName = 'html-part';

    var emitHtmlPart = function($el) {
        if ($el.hasClass(switchClassName))
            return false;

        app.emit('htmlPart', $el);
        $el.addClass(switchClassName);
    };
    parts.render = function(id, context, cb) {
        if (typeof id !== 'string')
            return cb(app.error('Don`t correct id for render'));

        async.waterfall([
            function(cb) {
                app.backend.emit('parts', {
                    id: id,
                    context: ((typeof context === 'object') ? context : {})
                }, cb);
            },
            function(data, cb) {
                if (data.html)
                    data.$el = $(data.html);

                emitHtmlPart(data.$el);
                cb(null, data);
            }
        ], cb);
    };

    setTimeout(function () {
        if (app.backend.socket.connected) {
            emitHtmlPart($('body'));
        } else {
            app.backend.on('connect', function() {
                emitHtmlPart($('body'));
            });
        };
    }, 1000);

})(app);