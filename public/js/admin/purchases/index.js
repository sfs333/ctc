(function(app) {
    app.on('load', function () {
        var $table = $('.admin-purchases-table');
        $table.tablesorter({
            theme          : 'dark',
            widgets        : ['zebra', 'columns'],
            usNumberFormat : false,
            sortReset      : true,
            sortRestart    : true
        });
    });
})(app);