(function(app) {
    var randomScalingFactor = function(){ return Math.round(Math.random()*5000)};
    var lineChartData = {
        labels : ["January","February","March","April","May","June","July","August","September","October","November","December"],
        datasets : [
            {
                label: "Site income",
                fillColor : "rgba(220,220,220,0.2)",
                strokeColor : "rgba(220,220,220,1)",
                pointColor : "rgba(220,220,220,1)",
                pointStrokeColor : "#fff",
                pointHighlightFill : "#fff",
                pointHighlightStroke : "rgba(220,220,220,1)",
                data : [
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor(),
                    randomScalingFactor()
                ]
            }
            //,
            //{
            //    label: "My Second dataset",
            //    fillColor : "rgba(151,187,205,0.2)",
            //    strokeColor : "rgba(151,187,205,1)",
            //    pointColor : "rgba(151,187,205,1)",
            //    pointStrokeColor : "#fff",
            //    pointHighlightFill : "#fff",
            //    pointHighlightStroke : "rgba(151,187,205,1)",
            //    data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
            //}
        ]

    }
    app.on('load', function () {
        var ctx = document.getElementById("canvas").getContext("2d");
        new Chart(ctx).Line(lineChartData, {
            responsive: true
        });
    });
})(app);