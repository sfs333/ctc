(function(app) {
    var popup = app.module('popup');
    var messages = {
        removeUser: 'Are you sure?'
    }
    app.on('load', function () {
        var $usersTable = $('.view-user-adminList table');

        var deleteEvents = function(wrap) {
            wrap.find('.link-user-delete').on('click', function (e) {
                var $self = $(this);
                popup.confirm(messages.removeUser, function() {
                    var id = $self.data('id');
                    if (!id) return;

                    app.backend.emit('deleteUser', {id: id}, function(err, response) {
                        if (err) return console.error(err);

                        app.action('redirect', {url: ''});
                    });
                }, function() {}, {width: 300});
                e.preventDefault();
                return false;
            });
        }
        var payEvents = function(wrap) {
            var $payItems = wrap.find('.user-pay-field.boolean');
            $payItems.on('click', function(e) {
                e.preventDefault();
                var $item = $(this);
                var $link = $item.find('a');
                if ($link.hasClass('disabled'))
                    return false;
                $link.addClass('disabled');
                var status = Boolean($item.hasClass('bool-false'));
                var id = $item.parents('tr').data('itemid');

                if (!id)
                    return console.error('User id not find');

                app.backend.emit('setPayStatus', {id: id, status: status}, function(err) {
                    if (err) return console.error(err);

                    $link.removeClass('disabled');
                    $item.removeClass(status ? 'bool-false' : 'bool-true');
                    $item.addClass(status ? 'bool-true' : 'bool-false');
                    $link.find('.field-value').text(status ? 'Yes' : 'No');
                });
                return false;
            });
        }
        var attachEvents = function(wrap) {
            deleteEvents(wrap);
            payEvents(wrap);
        }
        attachEvents($usersTable);
        app.on('update-view-user-adminList', function(wrap) {
            attachEvents(wrap);
        });

    });
})(app);