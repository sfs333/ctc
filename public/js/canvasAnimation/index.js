(function (app) {

    var animateSprite = function (opt, callback) {
        this.cImageTimeout = false;
        this.genImage = {};
        this.g_GameObjectManager = null;
        this.params = {
            cSpeed: 2,
            cWidth: 50,
            cHeight: 50,
            cTotalFrames: 12,
            cFrameWidth: 50
        }
        this.construct(opt, callback);
    }

    animateSprite.prototype.construct = function(opt, callback) {
        if (typeof callback != 'function')
            callback = function(err) { if (err) console.error(err); }

        if (typeof opt != 'object')
            return callback(app.error('Not correct first argument in animateSprite method, it should be object'));

        if (!opt.src)
            return callback(app.error('Not find src option'));

        if (!opt.wrapper && (typeof opt.wrapper == 'string'))
            return callback(app.error('Not find wrapper option'));

        for (var param in opt)
            this.params[param] = opt[param];

        this.params.wrapper = opt.wrapper;
        this.canvasHtml = '<canvas width="' + this.params.cWidth + '" height="' + this.params.cHeight + '"><p>Your browser does not support the canvas element.</p></canvas>';
        new this.imageLoader(opt.src, this, callback);
    }

    animateSprite.prototype.startAnimation = function(callback) {

        var wrapper = (typeof this.params.wrapper == 'object') ? this.params.wrapper : document.getElementById(this.params.wrapper);
        var animateClass = this;
        if (!wrapper)
            return callback(app.error('element with id ' + wrapper + ' not find'));

        wrapper.innerHTML = this.canvasHtml;
        this.genImage.onload = function () {
            animateClass.cImageTimeout = setTimeout(function() {
                animateClass.startAnimation(callback);
            }, 0);
        };
        app.module('canvas').init(this.params, this.genImage, wrapper.childNodes[0], callback);
    }

    animateSprite.prototype.imageLoader = function(s, opject, callback) {
        clearTimeout(this.cImageTimeout);
        //console.log(this);
        opject.cImageTimeout = 0;
        opject.genImage = new Image();
        opject.genImage.onload = function () {
            opject.cImageTimeout = setTimeout(function() {
                opject.startAnimation(callback);
            }, 0);
        };
        opject.genImage.onerror = new Function('alert(\'Could not load the image\')');
        opject.genImage.src = s;
    }


    app.module('canvas').animateSprite = function(opt, callback) {
        opt.src = opt.src || '/files/preloaders/circle.png';
        new animateSprite(opt, callback);
    }

    /* EXAMPLES */
    //app.modules.canvas.animateSprite({
    //    src: 'files/preloaders/circle.png',
    //    wrapper: 'loaderImage',
    //    cSpeed: 2
    //}, function(err, GameObjectManager) {
    //    if (err) return console.error(err);
    //    console.log(GameObjectManager);
    //});
    //app.modules.canvas.animateSprite({
    //    src: 'files/preloaders/circle.png',
    //    wrapper: 'loaderImage2',
    //    cSpeed: 5
    //}, function(err, GameObjectManager) {
    //    if (err) return console.error(err);
    //    console.log(GameObjectManager);
    //});
})(app);
