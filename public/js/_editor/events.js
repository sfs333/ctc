(function(app) {
    var editor = app.module('editor');
    editor.events = {
        onImageUpload: function(files) {
            var $field = $(this), i;
            if (files.length <= 0)
                return false;

            for (i = 0; i < files.length; i++) {
                editor.uploadFile($field, files[i], function(err, info) {
                    if (err)
                        return console.error(err);

                    $field.summernote('editor.insertImage', ('/' + info.path), info.name);
                });
            };
        },
        onMediaDelete: function($file) {
            console.log('REMOVE FILE WITH NAME ' + $file.attr('data-filename'));
        }
    }
})(app);