(function(app) {
    var editor = app.module('editor');
    editor.uploadFile = function($field, file, cb) {
        var $item = $field.parents('.form-item');
        var $form = $item.parents('form');
        var stream = ss.createStream();
        var size = 0;
        var blobStream;
        var $progressBar = $('<i class="fa fa-refresh fa-spin"></i>');

        $field.summernote('editor.insertNode', $progressBar[0]);

        ss(app.backend.socket).emit('file', stream, {
            formID: $form.attr('id'),
            fieldName: $field.attr('name'),
            size: file.size,
            name: file.name,
            type: file.type
        }, function(response) {
            if (!response.status)
                app.action('formErrorValidate', {message: response.message, field: $field.attr('name')}, $form);
            $progressBar.remove();
            stream.destroy();
            delete blobStream;
            cb((response.status ? null : app.error(response.message)), {
                path: response.path || null,
                name: response.name || null
            });
        });

        blobStream = ss.createBlobReadStream(file);
        blobStream.on('data', function(chunk) {
            size += chunk.length;
            //$counter.text(Number(((size / file.size) * 100).toFixed()));
        });

        blobStream.pipe(stream);
    }
})(app);