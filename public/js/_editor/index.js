(function(app) {
    var editor = app.module('editor', {});
    editor.attach = function($el) {
        $el.summernote($.extend(editor.config, editor.events));
    }
})(app);