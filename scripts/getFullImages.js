var fs = require('fs.extra'),
    async = require('async'),
    path = require('path'),
    dirCatigories = 'public/files/albums',
    dirSources = 'public/files/sources';

var copySourceFile = function(dir, name, filePath, cb) {
    var fileSourcePath = path.join(dirSources, name);
    async.waterfall([
        function(cb) {
            fs.exists(fileSourcePath, function(exist) {
                cb((exist ? null : new Error('Not exist ' + fileSourcePath)));
            });
        },
        function(cb) {
            fs.copy(fileSourcePath, filePath, { replace: true }, cb);
        }
    ], cb);
}
var scanAndSaveDir = function(dir, cb) {
    async.waterfall([
        function(cb) {
            fs.readdir(dir, cb);
        },
        function(dirs, cb) {
            var workers = [],
                worker = function(dir, name, filePath) {
                    workers.push(function(cb) {
                        copySourceFile(dir, name, filePath, cb);
                    });
                }
            dirs.forEach(function(name) {
                var filePath = path.join(dir, name);
                if (fs.lstatSync(filePath).isFile())
                    worker(dir, name, filePath)
            });
            async.parallel(workers, cb);
        }
    ], cb);

}
async.waterfall([
    function(cb) {
        fs.readdir(dirCatigories, cb);
    },
    function(dirs, cb) {
        var workers = [],
        worker = function(dirPath) {
            workers.push(function(cb) {
                scanAndSaveDir(dirPath, cb);
            });
        }
        dirs.forEach(function(dirName) {
            var dirPath = path.join(dirCatigories, dirName, 'full');
            if (fs.lstatSync(dirPath).isDirectory())
                worker(dirPath)
        });
        async.parallel(workers, cb);
    }
], function(err) {
    if (err) return console.error(err);

    console.log("OK");
});
